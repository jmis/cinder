using System;
using System.Collections.Generic;
using System.Diagnostics;
using Cinder.Compiler;
using Cinder.Runtime;

namespace Cinder.Console
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Compilation.CompileFile("Library/src/bootstrap.lsp");
			ReadEvaluatePrintLoop();
		}

		private static void ReadEvaluatePrintLoop()
		{
			while (true)
			{
				try
				{
					System.Console.Write("==> ");
					var function = Compilation.CreateFunctionFromSource(System.Console.ReadLine());
					var stopwatch = Stopwatch.StartNew();
					var result = SafeExecute(function);
					stopwatch.Stop();
					System.Console.WriteLine(result);
					System.Console.WriteLine(stopwatch.ElapsedMilliseconds + "ms");
				}
				catch (Exception e)
				{
					System.Console.WriteLine(e.Message);
				}
			}
		}

		private static object SafeExecute(IFunction function)
		{
			try
			{
				return function.Apply();
			}
			catch (Exception e)
			{
				System.Console.WriteLine(e.Message);
				return null;
			}
		}
	}
}