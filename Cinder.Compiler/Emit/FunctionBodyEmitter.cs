using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using Cinder.Compiler.Expressions;
using Cinder.Compiler.Expressions.Constants;
using Cinder.Compiler.Expressions.ExceptionHandling;
using Cinder.Parsing;
using Cinder.Runtime;
using Cinder.Runtime.Interop.Invocation;
using Cinder.Runtime.Interop.Reflection;
using Cinder.Runtime.Sequences;

namespace Cinder.Compiler.Emit
{
	public class FunctionBodyEmitter : IExpressionVisitor
	{
		private readonly ILGenerator _generator;
		private readonly Dictionary<IExpression, Type> _knownFunctions;
		private readonly List<FieldBuilder> _fieldsForCapturedValues;
		private readonly Dictionary<string, FieldBuilder> _varFields;
		private readonly List<IExpression> _tailCalls;
		private readonly Dictionary<string, LocalBuilder> _locals;

		public FunctionBodyEmitter(ILGenerator generator, Dictionary<IExpression, Type> knownFunctions, List<FieldBuilder> fields, Dictionary<string, FieldBuilder> varFields, List<IExpression> tailCalls)
		{
			_generator = generator;
			_knownFunctions = knownFunctions;
			_fieldsForCapturedValues = fields;
			_varFields = varFields;
			_tailCalls = tailCalls;
			_locals = new Dictionary<string, LocalBuilder>();
		}

		public void EmitBody(FunctionExpression expression)
		{
			expression.Body.Accept(this);
			_generator.Emit(OpCodes.Ret);
		}

		public void Visit(ArgumentExpression expression)
		{
			_generator.EmitArgLoad(expression.Param.Index + 1);
		}

		public void Visit(CapturedVariableExpression expression)
		{
			_generator.Emit(OpCodes.Ldarg_0);
			_generator.Emit(OpCodes.Ldfld, _fieldsForCapturedValues.Find(f => f.Name == expression.Name));
		}

		public void Visit(ConditionalExpression expression)
		{
			var trueLabel = _generator.DefineLabel();
			var falseLabel = _generator.DefineLabel();
			var restLabel = _generator.DefineLabel();

			expression.ConditionalExpression1.Accept(this);
			_generator.Emit(OpCodes.Unbox_Any, typeof (bool));
			_generator.Emit(OpCodes.Brtrue, trueLabel);
			_generator.Emit(OpCodes.Br, falseLabel);

			_generator.MarkLabel(trueLabel);
			expression.TrueBranch.Accept(this);
			_generator.Emit(OpCodes.Br, restLabel);

			_generator.MarkLabel(falseLabel);
			expression.FalseBranch.Accept(this);
			_generator.Emit(OpCodes.Br, restLabel);

			_generator.MarkLabel(restLabel);
		}

		public void Visit(DefExpression expression)
		{
			_generator.Emit(OpCodes.Ldstr, expression.VarName);
			_generator.Emit(OpCodes.Call, typeof (Namespace).GetMethod("Create"));
			var varLocal = _generator.DeclareLocal(typeof (Var));
			_generator.EmitLocalStore(varLocal);

			expression.ValueExpression.Accept(this);
			var valueLocal = _generator.DeclareLocal(typeof (object));
			_generator.EmitLocalStore(valueLocal);

			_generator.EmitLocalLoad(varLocal);
			_generator.EmitLocalLoad(valueLocal);
			_generator.Emit(OpCodes.Callvirt, typeof (Var).GetMethod("Push"));
			_generator.Emit(OpCodes.Ldnull);
		}

		public void Visit(DefLocalExpression expression)
		{
			var localBuilder = _generator.DeclareLocal(typeof (object));
			_locals.Add(expression.VarName, localBuilder);
			expression.ValueExpression.Accept(this);
			_generator.EmitLocalStore(localBuilder);
			expression.Body.Accept(this);
		}

		public void Visit(FunctionExpression expression)
		{
			var functionType = _knownFunctions[expression];
			var functionLocal = _generator.DeclareLocal(functionType);
			_generator.Emit(OpCodes.Newobj, functionType.GetConstructors()[0]);
			_generator.EmitLocalStore(functionLocal);

			var argCounter = 1;

			foreach (var argumentBinding in expression.ScopeOfCreator.Arguments.Values)
			{
				_generator.EmitLocalLoad(functionLocal);
				_generator.EmitArgLoad(argCounter++);
				_generator.Emit(OpCodes.Stfld, functionType.GetField(argumentBinding.Name));
			}

			foreach (var localBinding in expression.ScopeOfCreator.Locals)
			{
				_generator.EmitLocalLoad(functionLocal);
				_generator.EmitLocalLoad(_locals[localBinding]);
				_generator.Emit(OpCodes.Stfld, functionType.GetField(localBinding));
			}

			_generator.EmitLocalLoad(functionLocal);
		}

		public void Visit(InstanceOfExpression expression)
		{
			expression.ObjExpression.Accept(this);
			_generator.Emit(OpCodes.Isinst, expression.Type);
		}

		public void Visit(InvocationExpression expression)
		{
			expression.FunctionExpression1.Accept(this);
			_generator.Emit(OpCodes.Castclass, typeof (IFunction));
			var signature = FunctionHelpers.CreateSignature(expression.ArgList.Count);
			var method = typeof (IFunction).GetMethod("Apply", signature);

			if (signature.Length <= FunctionHelpers.MaxArity)
			{
				foreach (var arg in expression.ArgList) arg.Accept(this);
			}
			else
			{
				for (var i = 0; i < FunctionHelpers.MaxArity; i++) expression.ArgList[i].Accept(this);
				var remainingArgs = expression.ArgList.Skip(FunctionHelpers.MaxArity).ToList();
				_generator.EmitArray(remainingArgs, expr => expr.Accept(this));
			}

			if (_tailCalls.Contains(expression)) _generator.EmitTailCall(OpCodes.Callvirt, method);
			else _generator.Emit(OpCodes.Callvirt, method);
		}

		public void Visit(LocalVariableExpression expression)
		{
			_generator.EmitLocalLoad(_locals[expression.Name]);
		}

		public void Visit(NewExpression expression)
		{
			var constructorSignature = typeof (object).Repeat(expression.ArgList.Count);
			var match = expression.TypeToNew.FindMatchingConstructor(constructorSignature);

			if (match != null)
			{
				for (var i = 0; i < expression.ArgList.Count; i++) expression.ArgList[i].Accept(this);
				_generator.Emit(OpCodes.Newobj, match);
			}
			else
			{
				_generator.EmitType(expression.TypeToNew);
				_generator.EmitArray(expression.ArgList, expr => expr.Accept(this));
				_generator.Emit(OpCodes.Call, typeof (FunctionBodyEmitter).GetMethod("InvokeConstructor"));
			}
		}

		public static object InvokeConstructor(Type type, object[] args)
		{
			var result = MethodFinderFactory.CreateMethodFinder().FindConstructor(type, args.GetTypes());
			if (result == null) throw new Exception("Constructor not found for type: " + type.FullName);
			return result(args);
		}

		public void Visit(ObjectMethodInvocationExpression expression)
		{
			expression.CallTargetExpression.Accept(this);
			_generator.Emit(OpCodes.Ldstr, expression.MethodName);
			_generator.EmitArray(expression.Arguments, expr => expr.Accept(this));
			var invokeObjectMethod = typeof (FunctionBodyEmitter).GetMethod("InvokeObjectMethod");
			if (_tailCalls.Contains(expression)) _generator.EmitTailCall(OpCodes.Call, invokeObjectMethod);
			else _generator.Emit(OpCodes.Call, invokeObjectMethod);
		}

		public static object InvokeObjectMethod(object obj, string methodName, object[] args)
		{
			var result = MethodFinderFactory.CreateMethodFinder().FindMethod(obj.GetType(), methodName, args.GetTypes());
			if (result == null) throw new Exception("Method not found for type: " + obj.GetType().FullName);
			return result(obj, args);
		}

		public void Visit(QuoteExpression expression)
		{
			expression.QuotedObject.Accept(this);
		}

		public void Visit(StaticMethodInvocationExpression expression)
		{
			var type = expression.TypeContainingMethod;
			var methodSignature = typeof (object).Repeat(expression.Arguments.Count);
			var methodMatch = type.FindMatchingMethod(expression.MethodName, methodSignature, typeof (object));

			if (methodMatch != null)
			{
				for (var i = 0; i < expression.Arguments.Count; i++) expression.Arguments[i].Accept(this);
			}
			else
			{
				methodMatch = typeof (FunctionBodyEmitter).GetMethod("InvokeStaticMethod");
				_generator.EmitType(expression.TypeContainingMethod);
				_generator.Emit(OpCodes.Ldstr, expression.MethodName);
				_generator.EmitArray(expression.Arguments, expr => expr.Accept(this));
			}

			if (_tailCalls.Contains(expression)) _generator.EmitTailCall(OpCodes.Call, methodMatch);
			else _generator.Emit(OpCodes.Call, methodMatch);
		}

		public static object InvokeStaticMethod(Type targetType, string methodName, object[] args)
		{
			var result = MethodFinderFactory.CreateMethodFinder().FindMethod(targetType, methodName, args.GetTypes());
			if (result == null) throw new Exception("Method " + methodName + " not found for type: " + targetType.FullName);
			return result(null, args);
		}

		public void Visit(VarExpression expression)
		{
			_generator.Emit(OpCodes.Ldsfld, _varFields[expression.VarName]);
			_generator.Emit(OpCodes.Callvirt, typeof (Var).GetMethod("get_Current"));
		}

		public void Visit(BooleanExpression expression)
		{
			_generator.EmitBoxedBoolean(expression.Val);
		}

		public void Visit(IntegerExpression expression)
		{
			_generator.EmitBoxedInt(expression.Val);
		}

		public void Visit(ListExpression expression)
		{
			_generator.EmitArray(expression.ElementExpressions, expr => expr.Accept(this));
			var constructor = typeof (LispList).FindMatchingConstructor(new List<Type>() {typeof (object[])});
			_generator.Emit(OpCodes.Newobj, constructor);
		}

		public void Visit(MapExpression expression)
		{
			_generator.EmitArray(expression.Expressions.SelectMany(kvp => new[] { kvp.Key, kvp.Value }).ToList(), expr => expr.Accept(this));
			var method = typeof (Map).FindMatchingMethod("ParseMap", new List<Type>() { typeof(object[]) }, typeof(Map));
			_generator.Emit(OpCodes.Call, method);
		}

		public void Visit(NullExpression expression)
		{
			_generator.Emit(OpCodes.Ldnull);
		}

		public void Visit(StringExpression expression)
		{
			_generator.Emit(OpCodes.Ldstr, expression.Val);
		}

		public void Visit(VectorExpression expression)
		{
			_generator.EmitArray(expression.ElementExpressions, expr => expr.Accept(this));
			var constructor = typeof (LispVector).FindMatchingConstructor(new List<Type>() {typeof (object[])});
			_generator.Emit(OpCodes.Newobj, constructor);
		}

		public void Visit(SymbolExpression expression)
		{
			_generator.Emit(OpCodes.Ldstr, expression.Val.Name);
			_generator.Emit(OpCodes.Newobj, typeof (Symbol).GetConstructors()[0]);
		}

		public void Visit(DoubleExpression expression)
		{
			_generator.EmitBoxedDouble(expression.Val);
		}

		public void Visit(RecurExpression expression)
		{
			_generator.EmitArgLoad(0);
			foreach (var argument in expression.Arguments) argument.Accept(this);
			var signature = FunctionHelpers.CreateSignature(expression.Arguments.Count);

			if (_tailCalls.Contains(expression)) _generator.EmitTailCall(OpCodes.Callvirt, typeof(IFunction).GetMethod("Apply", signature));
			else _generator.Emit(OpCodes.Callvirt, typeof(IFunction).GetMethod("Apply", signature));
		}

		public void Visit(TryExpression expression)
		{
			var tryCatchBlockLabel = _generator.BeginExceptionBlock();
			var result = _generator.DeclareLocal(typeof(object));
			expression.Body.Accept(this);
			_generator.EmitLocalStore(result);
			_generator.Emit(OpCodes.Leave, tryCatchBlockLabel);

			foreach (var catchExpression in expression.CatchExpressions)
			{
				_generator.BeginCatchBlock(catchExpression.ExceptionType);

				var exceptionLocal = _generator.DeclareLocal(typeof(object));
				_locals.Add(catchExpression.ExceptionIdentifier, exceptionLocal);
				_generator.EmitLocalStore(exceptionLocal);

				catchExpression.Body.Accept(this);
				_generator.EmitLocalStore(result);
				_generator.Emit(OpCodes.Leave, tryCatchBlockLabel);

				_locals.Remove(catchExpression.ExceptionIdentifier);
			}

			_generator.BeginFinallyBlock();
			expression.FinallyExpression.Accept(this);
			_generator.EndExceptionBlock();
			_generator.EmitLocalLoad(result);
		}

		public void Visit(NoOperationExpression expression)
		{
			
		}

		public void Visit(ThrowExpression expression)
		{
			expression.ExceptionExpression.Accept(this);
			_generator.Emit(OpCodes.Throw);
		}
	}
}