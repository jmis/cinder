using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Cinder.Compiler.Expressions;
using Cinder.Compiler.Parsing.Expression;
using Cinder.Parsing;
using Cinder.Runtime;
using Cinder.Runtime.Sequences;

namespace Cinder.Compiler.Emit
{
	public class NamespaceEmitter
	{
		private readonly ModuleBuilder _moduleBuilder;
		private readonly AssemblyBuilder _assemblyBuilder;

		public NamespaceEmitter(ModuleBuilder moduleBuilder, AssemblyBuilder assemblyBuilder)
		{
			_moduleBuilder = moduleBuilder;
			_assemblyBuilder = assemblyBuilder;
		}

		public void DumpNamespaceToFile()
		{
			_assemblyBuilder.Save("default.dll");
		}

		public static NamespaceEmitter CreateNamespaceCompiler(string namespaceName)
		{
			var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(namespaceName), AssemblyBuilderAccess.RunAndSave);
			var moduleBuilder = assemblyBuilder.DefineDynamicModule(namespaceName, namespaceName + ".dll");
			return new NamespaceEmitter(moduleBuilder, assemblyBuilder);
		}

		public IFunction CompileExpressionIntoFunction(IExpression expression)
		{
			var functionType = CompileFunction(
				new FunctionExpression(
					FunctionParameters.Empty,
					expression,
					new Scope(), false));

			return Activator.CreateInstance(functionType) as IFunction;
		}

		public Type CompileFunction(FunctionExpression expression)
		{
			var knownFunctions = new Dictionary<IExpression, Type>();

			foreach (var fexpr in DepthFirstTraversal.FindFunctions(expression))
			{
				var tailCalls = new TailPositionFinder().Find(fexpr);
				var functionTypeBuilder = _moduleBuilder.DefineType("AnonymousFunction_" + Guid.NewGuid().ToString("N"));
				var methodBuilder = CreateMethod(fexpr, functionTypeBuilder);
				var capturedVariablesFields = EmitCapturedVariables(functionTypeBuilder, fexpr.ScopeOfCreator);
				var varFields = EmitVarFields(functionTypeBuilder, fexpr);
				EmitVarInitializers(functionTypeBuilder, varFields);

				var constructor = functionTypeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new Type[0]);
				var constructorGenerator = constructor.GetILGenerator();
				constructorGenerator.Emit(OpCodes.Ret);

				var methodGenerator = methodBuilder.GetILGenerator();
				var functionEmitter = new FunctionBodyEmitter(methodGenerator, knownFunctions, capturedVariablesFields, varFields, tailCalls);
				functionEmitter.EmitBody(fexpr);
				knownFunctions.Add(fexpr, functionTypeBuilder.CreateType());
			}

			return knownFunctions[expression];
		}

		public MethodBuilder CreateMethod(FunctionExpression expression, TypeBuilder functionTypeBuilder)
		{
			MethodBuilder method = null;

			if (!expression.Parameters.IncludesVariableArity)
			{
				if (expression.IsMacro) functionTypeBuilder.SetParent(typeof (UserDefinedMacro));
				else functionTypeBuilder.SetParent(typeof (UserDefinedFunction));
				method = functionTypeBuilder.DefineMethod("Apply", MethodAttributes.Public | MethodAttributes.Virtual);
				var parameterTypes = new List<Type>();
				foreach (var parameter in expression.Parameters.Parameters) parameterTypes.Add(parameter.ParamType);
				method.SetParameters(parameterTypes.ToArray());
			}
			else
			{
				if (expression.IsMacro) functionTypeBuilder.SetParent(typeof (UserDefinedVarArgsMacro));
				else functionTypeBuilder.SetParent(typeof (UserDefinedVarArgsFunction));
				method = functionTypeBuilder.DefineMethod("ApplyVarArgs", MethodAttributes.Public | MethodAttributes.Virtual);
				var parameterTypes = new List<Type>();
				foreach (var parameter in expression.Parameters.Parameters) parameterTypes.Add(parameter.ParamType);
				method.SetParameters(parameterTypes.ToArray());

				var arityCountMethod = functionTypeBuilder.DefineMethod("GetMandatoryArity", MethodAttributes.Public | MethodAttributes.Virtual);
				arityCountMethod.SetReturnType(typeof(int));

				var generator = arityCountMethod.GetILGenerator();
				generator.EmitInt(expression.Parameters.Parameters.Count - 1);
				generator.Emit(OpCodes.Ret);
			}

			method.SetReturnType(typeof (object));
			return method;
		}

		private static List<FieldBuilder> EmitCapturedVariables(TypeBuilder typeBuilder, Scope creatorsScope)
		{
			var fields = new List<FieldBuilder>();

			foreach (var variableInScope in creatorsScope.Locals)
				fields.Add(typeBuilder.DefineField(variableInScope, typeof (object), FieldAttributes.Public));

			foreach (var variableInScope in creatorsScope.Arguments.Values)
				fields.Add(typeBuilder.DefineField(variableInScope.Name, typeof (object), FieldAttributes.Public));

			foreach (var variableInScope in creatorsScope.CapturedBindings)
				fields.Add(typeBuilder.DefineField(variableInScope, typeof (object), FieldAttributes.Public));

			return fields;
		}

		private static Dictionary<string, FieldBuilder> EmitVarFields(TypeBuilder functionTypeBuilder, FunctionExpression functionExpression)
		{
			var varFields = new Dictionary<string, FieldBuilder>();

			foreach (var varExpression in DepthFirstTraversal.FindVars(functionExpression))
			{
				if (varFields.ContainsKey(varExpression.VarName)) continue;
				var varField = functionTypeBuilder.DefineField(varExpression.VarName, typeof (Var), FieldAttributes.Public | FieldAttributes.Static);
				varFields.Add(varExpression.VarName, varField);
			}

			return varFields;
		}

		private static void EmitVarInitializers(TypeBuilder typeBuilder, Dictionary<string, FieldBuilder> varFields)
		{
			var staticInit = typeBuilder.DefineTypeInitializer();
			var generator = staticInit.GetILGenerator();

			foreach (var varName in varFields.Keys)
			{
				generator.Emit(OpCodes.Ldstr, varName);
				generator.Emit(OpCodes.Call, typeof (Namespace).GetMethod("Create"));
				generator.Emit(OpCodes.Stsfld, varFields[varName]);
			}

			generator.Emit(OpCodes.Ret);
		}
	}
}