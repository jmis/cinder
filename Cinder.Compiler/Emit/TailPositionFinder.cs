using System.Collections.Generic;
using Cinder.Compiler.Expressions;
using Cinder.Compiler.Expressions.Constants;
using Cinder.Compiler.Expressions.ExceptionHandling;
using Cinder.Parsing;

namespace Cinder.Compiler.Emit
{
	public class TailPositionFinder : IExpressionVisitor
	{
		private List<IExpression> _tailCallExpressions;
		
		public List<IExpression> Find(FunctionExpression root)
		{
			_tailCallExpressions = new List<IExpression>();
			root.Body.Accept(this);
			return _tailCallExpressions;
		}

		public void Visit(ArgumentExpression expression)
		{
			
		}

		public void Visit(CapturedVariableExpression expression)
		{

		}

		public void Visit(ConditionalExpression expression)
		{
			expression.TrueBranch.Accept(this);
			expression.FalseBranch.Accept(this);
		}

		public void Visit(DefExpression expression)
		{
			
		}

		public void Visit(DefLocalExpression expression)
		{
			
		}

		public void Visit(FunctionExpression expression)
		{
			
		}

		public void Visit(InstanceOfExpression expression)
		{
			
		}

		public void Visit(InvocationExpression expression)
		{
			_tailCallExpressions.Add(expression);
		}

		public void Visit(LocalVariableExpression expression)
		{
			
		}

		public void Visit(NewExpression expression)
		{
			
		}

		public void Visit(ObjectMethodInvocationExpression expression)
		{
			_tailCallExpressions.Add(expression);
		}

		public void Visit(QuoteExpression expression)
		{
			
		}

		public void Visit(StaticMethodInvocationExpression expression)
		{
			_tailCallExpressions.Add(expression);
		}

		public void Visit(VarExpression expression)
		{
			
		}

		public void Visit(BooleanExpression expression)
		{
			
		}

		public void Visit(IntegerExpression expression)
		{

		}

		public void Visit(ListExpression expression)
		{
			
		}

		public void Visit(NullExpression expression)
		{
			
		}

		public void Visit(StringExpression expression)
		{
			
		}

		public void Visit(VectorExpression expression)
		{
			
		}

		public void Visit(SymbolExpression expression)
		{
			
		}

		public void Visit(DoubleExpression expression)
		{
			
		}

		public void Visit(RecurExpression expression)
		{
			_tailCallExpressions.Add(expression);
		}

		public void Visit(TryExpression expression)
		{
			
		}

		public void Visit(NoOperationExpression expression)
		{
			
		}

		public void Visit(ThrowExpression expression)
		{
			
		}

		public void Visit(MapExpression expression)
		{

		}
	}
}
