using System.Collections.Generic;

namespace Cinder.Compiler.Expressions
{
	public class FunctionParameters
	{
		private readonly List<Parameter> _parameters;

		public FunctionParameters(List<Parameter> parameters)
		{
			_parameters = parameters;
		}

		public List<Parameter> Parameters
		{
			get { return _parameters; }
		}

		public bool IncludesVariableArity
		{
			get { return _parameters.Exists(p => p.IsForVariableArity); }
		}

		public static FunctionParameters Empty = new FunctionParameters(new List<Parameter>());
	}
}