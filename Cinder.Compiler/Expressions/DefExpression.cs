using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class DefExpression : IExpression
	{
		private readonly string _varName;
		private readonly IExpression _valueExpression;

		public DefExpression(string varName, IExpression valueExpression)
		{
			_varName = varName;
			_valueExpression = valueExpression;
		}

		public string VarName
		{
			get { return _varName; }
		}

		public IExpression ValueExpression
		{
			get { return _valueExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}