using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class NoOperationExpression : IExpression
	{
		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}
