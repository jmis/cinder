using System;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class InstanceOfExpression : IExpression
	{
		private readonly IExpression _objExpression;
		private readonly Type _type;

		public InstanceOfExpression(IExpression objExpression, Type type)
		{
			_objExpression = objExpression;
			_type = type;
		}

		public Type Type
		{
			get { return _type; }
		}

		public IExpression ObjExpression
		{
			get { return _objExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}