using System;
using System.Collections.Generic;
using Cinder.Compiler.Expressions;
using Cinder.Compiler.Expressions.Constants;
using Cinder.Compiler.Expressions.ExceptionHandling;
using Cinder.Parsing;

namespace Cinder.Compiler.Emit
{
	public class DepthFirstTraversal : IExpressionVisitor
	{
		private readonly Action<IExpression> _nodeTraversedAction;

		public DepthFirstTraversal(Action<IExpression> nodeTraversedAction)
		{
			_nodeTraversedAction = nodeTraversedAction;
		}

		public void Visit(ArgumentExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(CapturedVariableExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(ConditionalExpression expression)
		{
			expression.ConditionalExpression1.Accept(this);
			expression.FalseBranch.Accept(this);
			expression.TrueBranch.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(DefExpression expression)
		{
			expression.ValueExpression.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(DefLocalExpression expression)
		{
			expression.ValueExpression.Accept(this);
			expression.Body.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(FunctionExpression expression)
		{
			expression.Body.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(InstanceOfExpression expression)
		{
			expression.ObjExpression.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(InvocationExpression expression)
		{
			expression.ArgList.ForEach(a => a.Accept(this));
			expression.FunctionExpression1.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(LocalVariableExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(NewExpression expression)
		{
			expression.ArgList.ForEach(a => a.Accept(this));
			_nodeTraversedAction(expression);
		}

		public void Visit(ObjectMethodInvocationExpression expression)
		{
			expression.CallTargetExpression.Accept(this);
			expression.Arguments.ForEach(a => a.Accept(this));
			_nodeTraversedAction(expression);
		}

		public void Visit(QuoteExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(StaticMethodInvocationExpression expression)
		{
			expression.Arguments.ForEach(a => a.Accept(this));
			_nodeTraversedAction(expression);
		}

		public void Visit(VarExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(BooleanExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(IntegerExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(ListExpression expression)
		{
			foreach (var elementExpression in expression.ElementExpressions) elementExpression.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(NullExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(StringExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(VectorExpression expression)
		{
			foreach (var elementExpression in expression.ElementExpressions) elementExpression.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(MapExpression expression)
		{
			foreach (var kvp in expression.Expressions)
			{
				kvp.Key.Accept(this);
				kvp.Value.Accept(this);
			}

			_nodeTraversedAction(expression);
		}

		public void Visit(SymbolExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(DoubleExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(RecurExpression expression)
		{
			foreach (var argument in expression.Arguments) argument.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(TryExpression expression)
		{
			expression.Body.Accept(this);
			expression.FinallyExpression.Accept(this);
			_nodeTraversedAction(expression);
		}

		public void Visit(NoOperationExpression expression)
		{
			_nodeTraversedAction(expression);
		}

		public void Visit(ThrowExpression expression)
		{
			expression.ExceptionExpression.Accept(this);
			_nodeTraversedAction(expression);
		}

		public static List<FunctionExpression> FindFunctions(IExpression root)
		{
			return GetAllExpressions(root)
				.FindAll(e => e is FunctionExpression)
				.ConvertAll(e => e as FunctionExpression);
		}

		public static List<VarExpression> FindVars(IExpression root)
		{
			return GetAllExpressions(root)
				.FindAll(e => e is VarExpression)
				.ConvertAll(e => e as VarExpression);
		}

		public static List<IExpression> GetAllExpressions(IExpression root)
		{
			var expressions = new List<IExpression>();
			root.Accept(new DepthFirstTraversal(expressions.Add));
			return expressions;
		}
	}
}