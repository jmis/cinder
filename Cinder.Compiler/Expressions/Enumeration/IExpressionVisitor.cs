using Cinder.Compiler.Expressions;
using Cinder.Compiler.Expressions.Constants;
using Cinder.Compiler.Expressions.ExceptionHandling;

namespace Cinder.Parsing
{
	public interface IExpressionVisitor
	{
		void Visit(ArgumentExpression expression);
		void Visit(CapturedVariableExpression expression);
		void Visit(ConditionalExpression expression);
		void Visit(DefExpression expression);
		void Visit(DefLocalExpression expression);
		void Visit(FunctionExpression expression);
		void Visit(InstanceOfExpression expression);
		void Visit(InvocationExpression expression);
		void Visit(LocalVariableExpression expression);
		void Visit(NewExpression expression);
		void Visit(ObjectMethodInvocationExpression expression);
		void Visit(QuoteExpression expression);
		void Visit(StaticMethodInvocationExpression expression);
		void Visit(VarExpression expression);
		void Visit(BooleanExpression expression);
		void Visit(IntegerExpression expression);
		void Visit(ListExpression expression);
		void Visit(NullExpression expression);
		void Visit(StringExpression expression);
		void Visit(VectorExpression expression);
		void Visit(MapExpression expression);
		void Visit(SymbolExpression expression);
		void Visit(DoubleExpression expression);
		void Visit(RecurExpression expression);
		void Visit(TryExpression expression);
		void Visit(NoOperationExpression expression);
		void Visit(ThrowExpression expression);
	}
}