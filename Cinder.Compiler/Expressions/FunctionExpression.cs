using Cinder.Compiler.Parsing.Expression;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class FunctionExpression : IExpression
	{
		private readonly FunctionParameters _parameters;
		private readonly IExpression _body;
		private readonly Scope _scopeOfCreator;
		private readonly bool _isMacro;

		public FunctionExpression(FunctionParameters parameters, IExpression body, Scope scopeOfCreator, bool isMacro)
		{
			_parameters = parameters;
			_body = body;
			_scopeOfCreator = scopeOfCreator;
			_isMacro = isMacro;
		}

		public bool IsMacro
		{
			get { return _isMacro; }
		}

		public Scope ScopeOfCreator
		{
			get { return _scopeOfCreator; }
		}

		public IExpression Body
		{
			get { return _body; }
		}

		public FunctionParameters Parameters
		{
			get { return _parameters; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}