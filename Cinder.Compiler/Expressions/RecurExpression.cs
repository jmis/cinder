using System.Collections.Generic;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class RecurExpression : IExpression
	{
		private readonly List<IExpression> _arguments;

		public RecurExpression(List<IExpression> arguments)
		{
			_arguments = arguments;
		}

		public List<IExpression> Arguments
		{
			get { return _arguments; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}