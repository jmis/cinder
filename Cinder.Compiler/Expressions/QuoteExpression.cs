using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class QuoteExpression : IExpression
	{
		private readonly IExpression _quotedObject;

		public QuoteExpression(IExpression quotedObject)
		{
			_quotedObject = quotedObject;
		}

		public IExpression QuotedObject
		{
			get { return _quotedObject; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}