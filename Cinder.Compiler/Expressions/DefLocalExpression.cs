using Cinder.Compiler.Parsing.Expression;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class DefLocalExpression : IExpression
	{
		private readonly IExpression _valueExpression;
		private readonly IExpression _body;
		private readonly string _varName;

		public DefLocalExpression(IExpression valueExpression, IExpression body, string varName)
		{
			_valueExpression = valueExpression;
			_body = body;
			_varName = varName;
		}

		public string VarName
		{
			get { return _varName; }
		}

		public IExpression Body
		{
			get { return _body; }
		}

		public IExpression ValueExpression
		{
			get { return _valueExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}