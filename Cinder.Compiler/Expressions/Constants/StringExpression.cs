using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.Constants
{
	public class StringExpression : IExpression
	{
		private readonly string _val;

		public StringExpression(string val)
		{
			_val = val;
		}

		public string Val
		{
			get { return _val; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}