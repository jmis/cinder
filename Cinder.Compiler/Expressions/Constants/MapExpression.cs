using System;
using Cinder.Compiler.Expressions;
using Cinder.Parsing;
using System.Collections.Generic;

namespace Cinder.Compiler.Expressions.Constants
{
	public class MapExpression : IExpression
	{
		private Dictionary<IExpression, IExpression> _expressions;

		public MapExpression (Dictionary<IExpression, IExpression> expressions)
		{
			_expressions = expressions;
		}

		public Dictionary<IExpression, IExpression> Expressions
		{
			get { return _expressions; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}

