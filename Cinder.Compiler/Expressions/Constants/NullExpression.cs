using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.Constants
{
	public class NullExpression : IExpression
	{
		public NullExpression()
		{
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}