using Cinder.Parsing;
using Cinder.Runtime;

namespace Cinder.Compiler.Expressions.Constants
{
	public class SymbolExpression : IExpression
	{
		private readonly Symbol _val;

		public SymbolExpression(Symbol val)
		{
			_val = val;
		}

		public Symbol Val
		{
			get { return _val; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}