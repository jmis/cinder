using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.Constants
{
	public class IntegerExpression : IExpression
	{
		private readonly int _val;

		public IntegerExpression(int val)
		{
			_val = val;
		}

		public int Val
		{
			get { return _val; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}