using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.Constants
{
	public class DoubleExpression : IExpression
	{
		private readonly double _val;

		public DoubleExpression(double val)
		{
			_val = val;
		}

		public double Val
		{
			get { return _val; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}