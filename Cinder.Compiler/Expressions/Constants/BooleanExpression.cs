using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.Constants
{
	public class BooleanExpression : IExpression
	{
		private readonly bool _val;

		public BooleanExpression(bool val)
		{
			_val = val;
		}

		public bool Val
		{
			get { return _val; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}