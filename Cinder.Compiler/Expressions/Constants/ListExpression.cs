using System.Collections.Generic;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.Constants
{
	public class ListExpression : IExpression
	{
		private readonly List<IExpression> _elementExpressions;

		public ListExpression(List<IExpression> elementExpressions)
		{
			_elementExpressions = elementExpressions;
		}

		public List<IExpression> ElementExpressions
		{
			get { return _elementExpressions; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}