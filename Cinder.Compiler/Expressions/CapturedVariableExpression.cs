using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class CapturedVariableExpression : IExpression
	{
		private readonly string _name;

		public CapturedVariableExpression(string name)
		{
			_name = name;
		}

		public string Name
		{
			get { return _name; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}