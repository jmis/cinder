using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public interface IExpression
	{
		void Accept(IExpressionVisitor visitor);
	}
}