using System;
using System.Collections.Generic;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class NewExpression : IExpression
	{
		private readonly Type _typeToNew;
		private readonly List<IExpression> _argList;

		public NewExpression(Type typeToNew, List<IExpression> argList)
		{
			_argList = argList;
			_typeToNew = typeToNew;
		}

		public List<IExpression> ArgList
		{
			get { return _argList; }
		}

		public Type TypeToNew
		{
			get { return _typeToNew; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}