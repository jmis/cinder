using Cinder.Parsing;

namespace Cinder.Compiler.Expressions.ExceptionHandling
{
	public class ThrowExpression : IExpression
	{
		private readonly IExpression _exceptionExpression;

		public ThrowExpression(IExpression exceptionExpression)
		{
			_exceptionExpression = exceptionExpression;
		}

		public IExpression ExceptionExpression
		{
			get { return _exceptionExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}