using System;
using Cinder.Runtime;

namespace Cinder.Compiler.Expressions.ExceptionHandling
{
	public class CatchBlock
	{
		private readonly Type _exceptionType;
		private readonly string _exceptionIdentifier;
		private readonly IExpression _body;

		public CatchBlock(Type exceptionType, string exceptionIdentifier, IExpression body)
		{
			_exceptionType = exceptionType;
			_exceptionIdentifier = exceptionIdentifier;
			_body = body;
		}

		public string ExceptionIdentifier
		{
			get { return _exceptionIdentifier; }
		}

		public IExpression Body
		{
			get { return _body; }
		}

		public Type ExceptionType
		{
			get { return _exceptionType; }
		}
	}
}
