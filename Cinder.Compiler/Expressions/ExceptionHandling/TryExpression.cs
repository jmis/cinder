using System.Collections.Generic;
using Cinder.Compiler.Expressions.ExceptionHandling;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class TryExpression : IExpression
	{
		private readonly IExpression _body;
		private readonly List<CatchBlock> _catchExpressions;
		private readonly IExpression _finallyExpression;

		public TryExpression(IExpression body, List<CatchBlock> catchExpressions, IExpression finallyExpression)
		{
			_body = body;
			_catchExpressions = catchExpressions;
			_finallyExpression = finallyExpression;
		}

		public IExpression FinallyExpression
		{
			get { return _finallyExpression; }
		}

		public List<CatchBlock> CatchExpressions
		{
			get { return _catchExpressions; }
		}

		public IExpression Body
		{
			get { return _body; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}