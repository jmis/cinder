using System;

namespace Cinder.Compiler.Expressions
{
	public class Parameter
	{
		private readonly string _name;
		private readonly int _index;
		private readonly Type _type;
		private readonly bool _isForVariableArity;

		public Parameter(string name, int index, Type type, bool isForVariableArity)
		{
			_name = name;
			_index = index;
			_type = type;
			_isForVariableArity = isForVariableArity;
		}

		public int Index
		{
			get { return _index; }
		}

		public bool IsForVariableArity
		{
			get { return _isForVariableArity; }
		}

		public Type ParamType
		{
			get { return _type; }
		}

		public string Name
		{
			get { return _name; }
		}
	}
}