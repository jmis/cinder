using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class ConditionalExpression : IExpression
	{
		private readonly IExpression _conditionalExpression;
		private readonly IExpression _trueBranch;
		private readonly IExpression _falseBranch;

		public ConditionalExpression(IExpression conditionalExpression, IExpression trueBranch, IExpression falseBranch)
		{
			_conditionalExpression = conditionalExpression;
			_trueBranch = trueBranch;
			_falseBranch = falseBranch;
		}

		public IExpression FalseBranch
		{
			get { return _falseBranch; }
		}

		public IExpression TrueBranch
		{
			get { return _trueBranch; }
		}

		public IExpression ConditionalExpression1
		{
			get { return _conditionalExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}