using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class VarExpression : IExpression
	{
		private readonly string _varName;

		public VarExpression(string varName)
		{
			_varName = varName;
		}

		public string VarName
		{
			get { return _varName; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}