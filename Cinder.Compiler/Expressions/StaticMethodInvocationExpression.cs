using System;
using System.Collections.Generic;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class StaticMethodInvocationExpression : IExpression
	{
		private readonly Type _typeContainingMethod;
		private readonly string _methodName;
		private readonly List<IExpression> _arguments;

		public StaticMethodInvocationExpression(Type typeContainingMethod, string methodName, List<IExpression> arguments)
		{
			_typeContainingMethod = typeContainingMethod;
			_methodName = methodName;
			_arguments = arguments;
		}

		public List<IExpression> Arguments
		{
			get { return _arguments; }
		}

		public string MethodName
		{
			get { return _methodName; }
		}

		public Type TypeContainingMethod
		{
			get { return _typeContainingMethod; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}