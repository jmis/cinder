using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class ArgumentExpression : IExpression
	{
		private readonly Parameter _parameter;

		public ArgumentExpression(Parameter parameter)
		{
			_parameter = parameter;
		}

		public Parameter Param
		{
			get { return _parameter; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}