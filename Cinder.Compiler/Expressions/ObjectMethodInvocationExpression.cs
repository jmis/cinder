using System.Collections.Generic;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class ObjectMethodInvocationExpression : IExpression
	{
		private readonly IExpression _callTargetExpression;
		private readonly string _methodName;
		private readonly List<IExpression> _arguments;

		public ObjectMethodInvocationExpression(IExpression callTargetExpression, string methodName, List<IExpression> arguments)
		{
			_callTargetExpression = callTargetExpression;
			_methodName = methodName;
			_arguments = arguments;
		}

		public List<IExpression> Arguments
		{
			get { return _arguments; }
		}

		public string MethodName
		{
			get { return _methodName; }
		}

		public IExpression CallTargetExpression
		{
			get { return _callTargetExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}