using System.Collections.Generic;
using Cinder.Parsing;

namespace Cinder.Compiler.Expressions
{
	public class InvocationExpression : IExpression
	{
		private readonly IExpression _functionExpression;
		private readonly List<IExpression> _argList;

		public InvocationExpression(IExpression functionExpression, List<IExpression> argList)
		{
			_functionExpression = functionExpression;
			_argList = argList;
		}

		public List<IExpression> ArgList
		{
			get { return _argList; }
		}

		public IExpression FunctionExpression1
		{
			get { return _functionExpression; }
		}

		public void Accept(IExpressionVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}