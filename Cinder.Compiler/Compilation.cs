using System.Collections.Generic;
using System.IO;
using Cinder.Compiler.Emit;
using Cinder.Compiler.IO;
using Cinder.Compiler.Parsing.Data;
using Cinder.Compiler.Parsing.Expression;
using Cinder.Compiler.Parsing.Lexical;
using Cinder.Runtime;

namespace Cinder.Compiler
{
	public class Compilation
	{
		private static readonly NamespaceEmitter DefaultNamespaceCompiler = NamespaceEmitter.CreateNamespaceCompiler("default");

		public static void CompileFile(string path)
		{
			foreach (var expression in ParseFileIntoData(path))
				CreateFunctionFromExpressionData(expression).Apply();
		}

		public static List<object> ParseFileIntoData(string sourceFile)
		{
			return new Parser(new Lexer(new PushBackCharacterStream(new StringReader(File.ReadAllText(sourceFile))))).Parse();
		}

		public static object ParseExpressionAsSourceIntoData(string source)
		{
			return new Parser(new Lexer(new PushBackCharacterStream(new StringReader(source)))).Parse()[0];
		}

		public static IFunction CreateFunctionFromSource(string source)
		{
			return CreateFunctionFromExpressionData(ParseExpressionAsSourceIntoData(source));
		}

		public static object Eval(object data)
		{
			return CreateFunctionFromExpressionData(data).Apply();
		}

		public static IFunction CreateFunctionFromExpressionData(object data)
		{
			var expressionParser = new ExpressionParser();
			var contextBindings = new Scope();
			foreach (var v in Namespace.GetAll()) contextBindings.Global.Add(v.Name, v);
			var expressionTree = expressionParser.ParseExpression(data, contextBindings);

			return DefaultNamespaceCompiler.CompileExpressionIntoFunction(expressionTree);
		}

		public static void DumpNamespaceToFile()
		{
			DefaultNamespaceCompiler.DumpNamespaceToFile();
		}
	}
}