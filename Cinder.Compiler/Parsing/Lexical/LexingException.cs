using System;

namespace Cinder.Compiler.Parsing.Lexical
{
	public class LexingException : Exception
	{
		public LexingException(string message) : base(message)
		{
			
		}
	}
}