using System;
using System.Collections.Generic;
using System.Text;
using Cinder.Compiler.IO;

namespace Cinder.Compiler.Parsing.Lexical
{
	public class Lexer
	{
		private static readonly List<char> ValidNonLetterSymbolPrefixes = new List<char>() {'%', '.', '*', '+', '!', '-', '_', '?', '>', '<', '=', '$', '&', '/'};

		private readonly PushBackCharacterStream _source;

		public Lexer(PushBackCharacterStream inputText)
		{
			_source = inputText;
		}

		public Token Next()
		{
			if (!_source.HasMore) return null;

			var currentChar = _source.Next();
			Token nextToken = null;

			if (currentChar == '(')
			{
				nextToken = new Token(TokenType.ListStart, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == ')')
			{
				nextToken = new Token(TokenType.ListEnd, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == '[')
			{
				nextToken = new Token(TokenType.VectorStart, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == ']')
			{
				nextToken = new Token(TokenType.VectorEnd, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == '{')
			{
				nextToken = new Token(TokenType.MapStart, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == '}')
			{
				nextToken = new Token(TokenType.MapEnd, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == '&')
			{
				nextToken = new Token(TokenType.VariableArity, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}
			else if (currentChar == ':')
			{
				_source.Push(currentChar);
				var keyword = ReadKeyword();
				nextToken = new Token(TokenType.Keyword, keyword, _source.CurrentIndex - keyword.Length, keyword.Length);
			}
			else if (IsPrefix(currentChar, "0x"))
			{
				ReadChars(1);
				var number = "0x" + ReadNumber();
				nextToken = new Token(TokenType.HexNumber, number, _source.CurrentIndex - number.Length, number.Length);
			}
			else if (Char.IsNumber(currentChar))
			{
				_source.Push(currentChar);
				var number = ReadNumber();
				if (IsInteger(number)) nextToken = new Token(TokenType.Integer, number, _source.CurrentIndex - number.Length, number.Length);
				else if (IsDouble(number)) nextToken = new Token(TokenType.Double, number, _source.CurrentIndex - number.Length, number.Length);
				else nextToken = new Token(TokenType.Unknown, number, _source.CurrentIndex - number.Length, number.Length);
			}
			else if (currentChar == '"')
			{
				_source.Push(currentChar);
				var str = ReadString();
				nextToken = new Token(TokenType.String, str, _source.CurrentIndex - str.Length, str.Length);
			}
			else if (IsWhitespace(currentChar))
			{
				_source.Push(currentChar);
				var str = ReadWhitespace();
				nextToken = new Token(TokenType.Whitespace, str, _source.CurrentIndex - str.Length, str.Length);
			}
			else if (currentChar == ';')
			{
				_source.Push(currentChar);
				var str = ReadComment();
				nextToken = new Token(TokenType.Comment, str, _source.CurrentIndex - str.Length, str.Length);
			}
			else if (IsString(currentChar, "true"))
			{
				ReadChars(3);
				nextToken = new Token(TokenType.Boolean, "true", _source.CurrentIndex - 4, 4);
			}
			else if (IsString(currentChar, "false"))
			{
				ReadChars(4);
				nextToken = new Token(TokenType.Boolean, "false", _source.CurrentIndex - 5, 5);
			}
			else if (IsString(currentChar, "null"))
			{
				ReadChars(3);
				nextToken = new Token(TokenType.Null, "null", _source.CurrentIndex - 4, 4);
			}
			else if (IsSymbolPrefix(currentChar))
			{
				_source.Push(currentChar);
				var str = ReadSymbol();
				nextToken = new Token(TokenType.Symbol, str, _source.CurrentIndex - str.Length, str.Length);
			}
			else
			{
				nextToken = new Token(TokenType.Unknown, currentChar.ToString(), _source.CurrentIndex - 1, 1);
			}

			return nextToken;
		}

		private static bool IsDouble(string s)
		{
			double d = 0;
			return double.TryParse(s, out d);
		}

		private static bool IsInteger(string s)
		{
			int i = 0;
			return int.TryParse(s, out i);
		}

		private static bool IsSymbolPrefix(char c)
		{
			return Char.IsLetter(c) || ValidNonLetterSymbolPrefixes.Contains(c);
		}

		private bool IsPrefix(char currentChar, string stringToMatch)
		{
			var str = new StringBuilder(currentChar.ToString());
			str.Append(ReadChars(stringToMatch.Length - 1));
			var nextChar = ReadChars(1);

			if (nextChar.Length > 0 && !IsDataStructureStart(nextChar[0]) && !IsTerminatingChar(nextChar[0]))
			{
				_source.Push(nextChar);
				var isMatch = str.ToString() == stringToMatch;
				_source.Push(str.ToString().Substring(1));
				return isMatch;
			}

			_source.Push(nextChar);
			_source.Push(str.ToString().Substring(1));
			return false;
		}

		private bool IsString(char currentChar, string stringToMatch)
		{
			var str = new StringBuilder(currentChar.ToString());
			str.Append(ReadChars(stringToMatch.Length - 1));
			var nextChar = ReadChars(1);

			if (nextChar.Length == 0 || IsDataStructureStart(nextChar[0]) || IsTerminatingChar(nextChar[0]))
			{
				_source.Push(nextChar);
				var isMatch = str.ToString() == stringToMatch;
				_source.Push(str.ToString().Substring(1));
				return isMatch;
			}

			_source.Push(nextChar);
			_source.Push(str.ToString().Substring(1));
			return false;
		}

		private string ReadChars(int charCount)
		{
			var chars = new StringBuilder();

			for (var i = 0; i < charCount; i++)
			{
				if (_source.HasMore) chars.Append(_source.Next());
				else return chars.ToString();
			}

			return chars.ToString();
		}

		private string ReadKeyword()
		{
			var parsedKeyword = new StringBuilder();
			var currentChar = _source.Next();

			while (!IsTerminatingChar(currentChar) && !IsDataStructureStart(currentChar))
			{
				parsedKeyword.Append(currentChar);
				if (_source.HasMore) currentChar = _source.Next();
				else return parsedKeyword.ToString();
			}

			_source.Push(currentChar);
			return parsedKeyword.ToString();
		}

		private string ReadSymbol()
		{
			var parsedSymbol = new StringBuilder();
			var currentChar = _source.Next();

			while (!IsTerminatingChar(currentChar))
			{
				parsedSymbol.Append(currentChar);
				if (_source.HasMore) currentChar = _source.Next();
				else return parsedSymbol.ToString();
			}

			_source.Push(currentChar);
			return parsedSymbol.ToString();
		}

		private string ReadComment()
		{
			return PutBackTrailingWhitespace(ReadToEndOfLineIncludingReturnCharacters());
		}

		private string PutBackTrailingWhitespace(string text)
		{
			for (var i = text.Length - 1; i >= 0; i--)
			{
				if (Char.IsWhiteSpace(text[i]))
				{
					_source.Push(text[i]);
				}
				else
				{
					return text.Substring(0, i + 1);
				}
			}

			return string.Empty;
		}

		private string ReadToEndOfLineIncludingReturnCharacters()
		{
			var parsedLine = new StringBuilder();
			var currentChar = _source.Next();

			while (currentChar != '\r' && currentChar != '\n')
			{
				parsedLine.Append(currentChar);
				if (_source.HasMore) currentChar = _source.Next();
				else return parsedLine.ToString();
			}

			parsedLine.Append(currentChar);

			if (currentChar == '\r')
			{
				currentChar = _source.Next();
				if (currentChar == '\n') parsedLine.Append(currentChar);
				else _source.Push(currentChar);
			}

			return parsedLine.ToString();
		}

		private string ReadWhitespace()
		{
			var parsedWhitespace = new StringBuilder();
			var currentChar = _source.Next();

			while (IsWhitespace(currentChar))
			{
				parsedWhitespace.Append(currentChar);
				if (_source.HasMore) currentChar = _source.Next();
				else return parsedWhitespace.ToString();
			}

			_source.Push(currentChar);
			return parsedWhitespace.ToString();
		}

		private string ReadNumber()
		{
			var parsedNumber = new StringBuilder();
			var currentChar = _source.Next();

			while (!IsTerminatingChar(currentChar))
			{
				parsedNumber.Append(currentChar);
				if (_source.HasMore) currentChar = _source.Next();
				else return parsedNumber.ToString();
			}

			_source.Push(currentChar);
			return parsedNumber.ToString();
		}

		private string ReadString()
		{
			var parsedString = new StringBuilder();
			var currentChar = _source.Next();
			parsedString.Append(currentChar);
			currentChar = _source.Next();
			var previousCharWasBackslash = false;

			while (currentChar != '"' || (currentChar == '"' && previousCharWasBackslash))
			{
				parsedString.Append(currentChar);
				previousCharWasBackslash = currentChar == '\\';
				if (_source.HasMore) currentChar = _source.Next();
				else return parsedString.ToString();
			}

			parsedString.Append(currentChar);
			return parsedString.ToString();
		}

		private static bool IsWhitespace(char c)
		{
			return Char.IsWhiteSpace(c) || c == ',';
		}

		private static bool IsTerminatingChar(char c)
		{
			return c == ')' || c == '}' || c == ']' || Char.IsWhiteSpace(c) || c == ';' || c == '"';
		}

		private static bool IsDataStructureStart(char c)
		{
			return c == '(' || c == '{' || c == '[';
		}
	}
}