using System;

namespace Cinder.Compiler.Parsing
{
	public class ParsingException : Exception
	{
		public ParsingException(string message) : base(message)
		{
		}
	}
}