using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cinder.Compiler.Expressions;
using Cinder.Runtime;
using Cinder.Runtime.Sequences;

namespace Cinder.Compiler.Parsing.Expression
{
	class FunctionParameterListParser
	{
		public static FunctionParameters Parse(Sequence symbolSequence)
		{
			var symbolList = new List<Symbol>();
			foreach (var symbol in symbolSequence) symbolList.Add(symbol as Symbol);

			var varArgSymbolCount = 0;
			foreach (var symbol in symbolList) if (symbol.Name == "&") varArgSymbolCount++;
			if (varArgSymbolCount > 1) throw new Exception("Cannot have more than one variable arity symbol in argument list.");

			var variableArityMarker = symbolList.Find(o => o.Name == "&");
			var hasVariableArity = variableArityMarker != null;

			if (hasVariableArity)
			{
				if (symbolList.IndexOf(variableArityMarker) != symbolList.Count - 2)
					throw new Exception("Variable arity symbol must be second last in argument list.");

				symbolList.Remove(variableArityMarker);
			}

			var variableAritySymbol = hasVariableArity ? symbolList[symbolList.Count - 1] : null;
			var parameters = new List<Parameter>();

			foreach (var parameterSymbol in symbolList)
			{
				if (parameterSymbol == variableAritySymbol) parameters.Add(new Parameter(parameterSymbol.Name, parameters.Count, typeof(Sequence), true));
				else parameters.Add(new Parameter(parameterSymbol.Name, parameters.Count, typeof(object), false));
			}

			return new FunctionParameters(parameters);
		}
	}
}
