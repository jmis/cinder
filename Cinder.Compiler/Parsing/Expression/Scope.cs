using System.Collections.Generic;
using Cinder.Compiler.Expressions;
using Cinder.Parsing;
using Cinder.Runtime;
using System.Linq;

namespace Cinder.Compiler.Parsing.Expression
{
	public class Scope
	{
		private readonly Dictionary<string, Var> _global;
		private readonly List<string> _locals;
		private readonly Dictionary<string, Parameter> _arguments;
		private readonly List<string> _capturedBindings;

		public Scope()
		{
			_global = new Dictionary<string, Var>();
			_capturedBindings = new List<string>();
			_locals = new List<string>();
			_arguments = new Dictionary<string, Parameter>();
		}

		public Scope(Dictionary<string, Var> global, List<string> locals, Dictionary<string, Parameter> arguments, List<string> capturedBindings)
		{
			_global = global;
			_locals = locals;
			_arguments = arguments;
			_capturedBindings = capturedBindings;
		}

		public Dictionary<string, Var> Global
		{
			get { return _global; }
		}

		public List<string> CapturedBindings
		{
			get { return _capturedBindings; }
		}

		public List<string> Locals
		{
			get { return _locals; }
		}

		public Dictionary<string, Parameter> Arguments
		{
			get { return _arguments; }
		}

		public bool Contains(string name)
		{
			return Global.ContainsKey(name) || Locals.Contains(name) || Arguments.ContainsKey(name) || _capturedBindings.Contains(name);
		}

		public Scope AddLocal(string name)
		{
			var localsWithAddition = new List<string>(_locals);
			localsWithAddition.Add(name);
			return new Scope(Global, localsWithAddition, Arguments, CapturedBindings);
		}

		public Scope MoveLocalsAndArgumentsToOuterScope()
		{
			var newScope = new Scope();
			foreach (var binding in Global) newScope.Global.Add(binding.Key, binding.Value);
			foreach (var binding in Locals) newScope.CapturedBindings.Add(binding);
			foreach (var binding in Arguments) newScope.CapturedBindings.Add(binding.Value.Name);
			foreach (var binding in CapturedBindings) newScope.CapturedBindings.Add(binding);
			return newScope;
		}

		public void AddArguments(FunctionParameters functionArguments)
		{
			foreach (var parameter in functionArguments.Parameters)
			{
				Arguments.Add(parameter.Name, parameter);
			}
		}
	}
}