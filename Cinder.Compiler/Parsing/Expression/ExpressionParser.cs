using System;
using System.Collections.Generic;
using Cinder.Compiler.Expressions;
using Cinder.Compiler.Expressions.Constants;
using Cinder.Compiler.Expressions.ExceptionHandling;
using Cinder.Runtime;
using Cinder.Runtime.Interop.TypeFinding;
using Cinder.Runtime.Sequences;

namespace Cinder.Compiler.Parsing.Expression
{
	public class ExpressionParser
	{
		public IExpression ParseExpression(object obj, Scope bindings)
		{
			if (obj == null) return new NullExpression();
			var objType = obj.GetType();
			if (objType == typeof (LispList)) return ParseList(obj as Sequence, bindings);
			if (objType == typeof (LispVector)) return ParseVector(obj as Sequence, bindings);
			if (objType == typeof (Map)) return ParseMap(obj as Map, bindings);
			if (objType == typeof (Symbol)) return ParseVarExpression(obj as Symbol, bindings);
			return ParseConstant(obj);
		}

		private IExpression ParseConstant(object obj)
		{
			if (obj == null) return new NullExpression();
			if (obj is bool) return new BooleanExpression((bool) obj);
			if (obj is string) return new StringExpression(obj as string);
			if (obj is int) return new IntegerExpression((int) obj);
			if (obj is double) return new DoubleExpression((double) obj);
			if (obj is LispVector) return ParseConstantVector(obj as Sequence);
			if (obj is LispList) return ParseConstantList(obj as Sequence);
			if (obj is Map) return ParseConstantMap(obj as Sequence);
			if (obj is Symbol) return new SymbolExpression(obj as Symbol);
			throw new ParsingException("Unknown constant type: " + obj.GetType().FullName);
		}

		private IExpression ParseConstantMap(Sequence sequence)
		{
			var expressions = new Dictionary<IExpression, IExpression>();

			foreach (KeyValuePair<object, object> kvp in sequence)
			{
				expressions.Add(ParseConstant(kvp.Key), ParseConstant(kvp.Value));
			}

			return new MapExpression(expressions);
		}

		private IExpression ParseConstantList(Sequence sequence)
		{
			var elementExpressions = new List<IExpression>();
			foreach (var element in sequence) elementExpressions.Add(ParseConstant(element));
			return new ListExpression(elementExpressions);
		}

		private IExpression ParseConstantVector(Sequence sequence)
		{
			var elementExpressions = new List<IExpression>();
			foreach (var element in sequence) elementExpressions.Add(ParseConstant(element));
			return new VectorExpression(elementExpressions);
		}

		private IExpression ParseMap(Map map, Scope bindings)
		{
			var expressions = new Dictionary<IExpression, IExpression>();

			foreach (KeyValuePair<object, object> kvp in map)
			{
				expressions.Add(ParseExpression(kvp.Key, bindings), ParseExpression(kvp.Value, bindings));
			}

			return new MapExpression(expressions);
		}

		private IExpression ParseVector(Sequence sequence, Scope bindings)
		{
			var elementExpressions = new List<IExpression>();
			foreach (var element in sequence) elementExpressions.Add(ParseExpression(element, bindings));
			return new VectorExpression(elementExpressions);
		}

		private IExpression ParseList(Sequence sequence, Scope bindings)
		{
			var functionExpression = sequence.First();

			if (functionExpression is Symbol)
			{
				var symbol = functionExpression as Symbol;
				if (symbol.Name == "def") return ParseDefExpression(sequence, bindings);
				if (symbol.Name == "new") return ParseNewExpression(sequence, bindings);
				if (symbol.Name == "fn") return ParseFunctionExpression(sequence, bindings);
				if (symbol.Name == "macro") return ParseMacroExpression(sequence, bindings);
				if (symbol.Name == "quote") return ParseQuoteExpression(sequence, bindings);
				if (symbol.Name == ".") return ParseDotExpression(sequence, bindings);
				if (symbol.Name == "if") return ParseConditionalExpression(sequence, bindings);
				if (symbol.Name == "def-local") return ParseDefLocalExpression(sequence, bindings);
				if (symbol.Name == "instance-of?") return ParseInstanceOfExpression(sequence, bindings);
				if (symbol.Name == "recur") return ParseRecurExpression(sequence, bindings);
				if (symbol.Name == "try") return ParseTryExpression(sequence, bindings);
				if (symbol.Name == "catch") throw new ParsingException("Unexpected catch keyword.");
				if (symbol.Name == "finally") throw new ParsingException("Unexpected finally keyword.");
				if (symbol.Name == "throw") return ParseThrowExpression(sequence, bindings);
			}

			return ParseInvocationExpression(sequence, bindings);
		}

		private IExpression ParseThrowExpression(Sequence sequence, Scope bindings)
		{
			return new ThrowExpression(ParseExpression(sequence.Rest().First(), bindings));
		}

		private IExpression ParseTryExpression(Sequence sequence, Scope bindings)
		{
			var body = ParseExpression(sequence.Rest().First(), bindings);
			var remainingExpressions = sequence.Rest().Rest();
			var catchExpressions = new List<CatchBlock>();

			if (remainingExpressions.CountSequence() == 0)
				throw new ParsingException("Expected catch block or finally block.");

			for (int i = 0; i < remainingExpressions.CountSequence(); i++)
			{
				var childObject = remainingExpressions[i];

				if (i < remainingExpressions.CountSequence()-1)
				{
					catchExpressions.Add(ParseCatchBlock(childObject, bindings));
				}
				else if (IsSequenceWhereTheFirstElementIsASymbolWithName(childObject, "catch"))
				{
					catchExpressions.Add(ParseCatchBlock(childObject, bindings));
				}
				else if (IsSequenceWhereTheFirstElementIsASymbolWithName(childObject, "finally"))
				{
					return new TryExpression(body, catchExpressions, ParseFinallyBlock(childObject, bindings));
				}
				else
				{
					throw new ParsingException("Expected catch or finally block.");
				}
			}

			var parameters = new FunctionParameters(new List<Parameter>());
			var innerFunctionScope = bindings.MoveLocalsAndArgumentsToOuterScope();
			innerFunctionScope.AddArguments(parameters);

			var tryExpression = new TryExpression(body, catchExpressions, new NoOperationExpression());
			var functionExpression = new FunctionExpression(parameters, tryExpression, bindings, false);
			return new InvocationExpression(functionExpression, new List<IExpression>());
		}

		private IExpression ParseFinallyBlock(object obj, Scope bindings)
		{
			if (!IsSequenceWhereTheFirstElementIsASymbolWithName(obj, "finally")) throw new ParsingException("Expecting finally expression.");
			var sequence = obj as Sequence;
			var body = ParseExpression(sequence.Rest().First(), bindings);
			return body;
		}

		private static bool IsSequenceWhereTheFirstElementIsASymbolWithName(object obj, string name)
		{
			var sequence = obj as Sequence;
			if (sequence == null) return false;
			var symbol = sequence.First() as Symbol;
			if (symbol == null) return false;
			if (symbol.Name != name) return false;
			return true;
		}

		private CatchBlock ParseCatchBlock(object obj, Scope bindings)
		{
			if (!IsSequenceWhereTheFirstElementIsASymbolWithName(obj, "catch")) throw new ParsingException("Expecting catch expression.");
			var sequence = obj as Sequence;
			var exceptionType = ParseTypeName(sequence.Rest().First());
			var exceptionIdentifier = sequence.Rest().Rest().First() as Symbol;
			if (exceptionIdentifier == null) throw new ParsingException("Excepted identifier for exception.");
			if (bindings.Contains(exceptionIdentifier.Name)) throw new ParsingException("Identifier " + exceptionIdentifier.Name + " already exists.");
			var body = ParseExpression(sequence.Rest().Rest().Rest().First(), bindings.AddLocal(exceptionIdentifier.Name));
			return new CatchBlock(exceptionType, exceptionIdentifier.Name, body);
		}

		private Type ParseTypeName(object obj)
		{
			var typeNameSymbol = obj as Symbol;
			if (typeNameSymbol == null) throw new ParsingException("Expected type name.");
			var foundType = TypeFinderFactory.CreateCachingTypeFinder().FindType(typeNameSymbol.Name);
			if (foundType == null) throw new ParsingException("Unknown type: " + typeNameSymbol.Name);
			return foundType;
		}

		private IExpression ParseRecurExpression(Sequence sequence, Scope bindings)
		{
			var argList = new List<IExpression>();
			foreach (var arg in sequence.Rest()) argList.Add(ParseExpression(arg, bindings));
			return new RecurExpression(argList);
		}

		private static IExpression ParseVarExpression(Symbol varName, Scope bindings)
		{
			if (!bindings.Contains(varName.Name)) throw new ParsingException("Unknown symbol: " + varName.Name);
			if (bindings.CapturedBindings.Contains(varName.Name)) return new CapturedVariableExpression(varName.Name);
			if (bindings.Locals.Contains(varName.Name)) return new LocalVariableExpression(varName.Name);
			if (bindings.Arguments.ContainsKey(varName.Name)) return new ArgumentExpression(bindings.Arguments[varName.Name]);
			return new VarExpression(varName.Name);
		}

		private IExpression ParseInstanceOfExpression(Sequence sequence, Scope bindings)
		{
			var typeSymbol = sequence.Rest().First() as Symbol;
			var foundType = TypeFinderFactory.CreateCachingTypeFinder().FindType(typeSymbol.Name);
			if (foundType == null) throw new ParsingException("Type not found: " + typeSymbol.Name);
			var objExpression = ParseExpression(sequence.Rest().Rest().First(), bindings);
			return new InstanceOfExpression(objExpression, foundType);
		}

		private IExpression ParseDefLocalExpression(Sequence sequence, Scope bindings)
		{
			var symbol = sequence.Rest().First() as Symbol;
			var valueExpression = ParseExpression(sequence.Rest().Rest().First(), bindings);
			var localBindings = bindings.AddLocal(symbol.Name);
			var body = ParseExpression(sequence.Rest().Rest().Rest().First(), localBindings);
			var expr = new DefLocalExpression(valueExpression, body, symbol.Name);
			return expr;
		}

		private IExpression ParseConditionalExpression(Sequence sequence, Scope bindings)
		{
			var conditionalExpression = ParseExpression(sequence.Rest().First(), bindings);
			var trueBranch = ParseExpression(sequence.Rest().Rest().First(), bindings);
			var falseBranch = ParseExpression(null, bindings);
			if (sequence.CountSequence() == 4) falseBranch = ParseExpression(sequence.Rest().Rest().Rest().First(), bindings);
			return new ConditionalExpression(conditionalExpression, trueBranch, falseBranch);
		}

		private IExpression ParseDotExpression(Sequence sequence, Scope bindings)
		{
			var callTarget = sequence.Rest().First();
			var methodNameSymbol = sequence.Rest().Rest().First() as Symbol;
			var argList = new List<IExpression>();
			foreach (var arg in sequence.Rest().Rest().Rest()) argList.Add(ParseExpression(arg, bindings));

			if (callTarget is Symbol)
			{
				var callTargetSymbol = callTarget as Symbol;
				var foundType = TypeFinderFactory.CreateCachingTypeFinder().FindType(callTargetSymbol.Name);

				if (foundType == null)
				{
					var callTargetExpression = ParseExpression(callTarget, bindings);
					return new ObjectMethodInvocationExpression(callTargetExpression, methodNameSymbol.Name, argList);
				}
				else
				{
					return new StaticMethodInvocationExpression(foundType, methodNameSymbol.Name, argList);
				}
			}
			else
			{
				var callTargetExpression = ParseExpression(callTarget, bindings);
				return new ObjectMethodInvocationExpression(callTargetExpression, methodNameSymbol.Name, argList);
			}
		}

		private IExpression ParseQuoteExpression(Sequence sequence, Scope bindings)
		{
			return new QuoteExpression(ParseConstant(sequence.Rest().First()));
		}

		private IExpression ParseInvocationExpression(Sequence sequence, Scope bindings)
		{
			var symbol = sequence.First() as Symbol;
			if (symbol == null) return CreateInvocationExpression(sequence, bindings);
			if (!bindings.Contains(symbol.Name)) throw new ParsingException("Unknown identifier: " + symbol.Name + ".");
			if (IsMacro(sequence, bindings)) return ExpandMacro(sequence, bindings);
			return CreateInvocationExpression(sequence, bindings);
		}

		private IExpression CreateInvocationExpression(Sequence sequence, Scope bindings)
		{
			var functionExpression = ParseExpression(sequence.First(), bindings);
			var argList = new List<IExpression>();
			foreach (var arg in sequence.Rest()) argList.Add(ParseExpression(arg, bindings));
			return new InvocationExpression(functionExpression, argList);
		}

		private bool IsMacro(Sequence sequence, Scope bindings)
		{
			var macroName = sequence.First() as Symbol;
			if (!bindings.Global.ContainsKey(macroName.Name)) return false;
			if (!bindings.Global[macroName.Name].IsBound) return false;
			var function = bindings.Global[macroName.Name].Current as IFunction;
			if (function == null) return false;
			return function.IsMacro();
		}

		private IExpression ExpandMacro(Sequence sequence, Scope bindings)
		{
			var macroName = sequence.First() as Symbol;
			var macro = bindings.Global[macroName.Name].Current as IFunction;
			var expandedMacro = FunctionHelpers.ApplyTo(macro, sequence.Rest().ConvertToList());
			var expandedExpression = ParseExpression(expandedMacro, bindings);
			return expandedExpression;
		}

		private IExpression ParseDefExpression(Sequence sequence, Scope bindings)
		{
			var symbolToDefine = sequence.Rest().First();
			var definingExpression = sequence.Rest().Rest().First();
			var newVar = new Var(symbolToDefine.ToString());
			if (!bindings.Global.ContainsKey(newVar.Name)) bindings.Global.Add(newVar.Name, newVar);
			return new DefExpression(newVar.Name, ParseExpression(definingExpression, bindings));
		}

		private IExpression ParseFunctionExpression(Sequence sequence, Scope bindings)
		{
			var argSequence = sequence.Rest().First() as Sequence;
			var parameters = FunctionParameterListParser.Parse(argSequence);
			var innerFunctionScope = bindings.MoveLocalsAndArgumentsToOuterScope();
			innerFunctionScope.AddArguments(parameters);

			var body = ParseExpression(sequence.Rest().Rest().First(), innerFunctionScope);
			var expression = new FunctionExpression(parameters, body, bindings, false);
			return expression;
		}

		private IExpression ParseMacroExpression(Sequence sequence, Scope bindings)
		{
			var argSequence = sequence.Rest().First() as Sequence;
			var parameters = FunctionParameterListParser.Parse(argSequence);
			var innerFunctionScope = bindings.MoveLocalsAndArgumentsToOuterScope();
			innerFunctionScope.AddArguments(parameters);

			var body = ParseExpression(sequence.Rest().Rest().First(), innerFunctionScope);
			var expression = new FunctionExpression(parameters, body, bindings, true);
			return expression;
		}

		private IExpression ParseNewExpression(Sequence sequence, Scope bindings)
		{
			var typeName = sequence.Rest().First() as Symbol;
			var argList = new List<IExpression>();
			foreach (var arg in sequence.Rest().Rest()) argList.Add(ParseExpression(arg, bindings));
			var foundType = TypeFinderFactory.CreateCachingTypeFinder().FindType(typeName.Name);
			return new NewExpression(foundType, argList);
		}
	}
}