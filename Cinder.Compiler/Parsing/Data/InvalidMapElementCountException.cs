using System;

namespace Cinder.Compiler
{
	public class InvalidMapElementCountException : Exception
	{
		public InvalidMapElementCountException () : base()
		{
		}
	}
}

