using System;
using System.Collections.Generic;
using System.Globalization;
using Cinder.Compiler.Parsing.Expression;
using Cinder.Compiler.Parsing.Lexical;
using Cinder.Parsing;
using Cinder.Runtime;
using Cinder.Runtime.Sequences;

namespace Cinder.Compiler.Parsing.Data
{
	public class Parser
	{
		private readonly Lexer _lexer;
		private readonly Stack<List<object>> _sequenceStack;
		private readonly Stack<TokenType> _sequenceTypeStack;

		public Parser(Lexer lexer)
		{
			_lexer = lexer;
			_sequenceStack = new Stack<List<object>>();
			_sequenceStack.Push(new List<object>());
			_sequenceTypeStack = new Stack<TokenType>();
			_sequenceTypeStack.Push(TokenType.ListStart);
		}

		public List<object> Parse()
		{
			var token = _lexer.Next();

			while (token != null)
			{
				if (token.Type == TokenType.ListStart)
				{
					_sequenceStack.Push(new List<object>());
					_sequenceTypeStack.Push(TokenType.ListStart);
				}
				else if (token.Type == TokenType.ListEnd)
				{
					if (_sequenceStack.Count == 1) throw new ParsingException("Unexpected token: " + token.Text);
					if (_sequenceTypeStack.Peek().MatchingBraceType() != token.Type) throw new ParsingException("Unexpected token: " + token.Text);
					var completedList = new LispList(_sequenceStack.Pop());
					_sequenceStack.Peek().Add(completedList);
					_sequenceTypeStack.Pop();
				}
				else if (token.Type == TokenType.VectorStart)
				{
					_sequenceStack.Push(new List<object>());
					_sequenceTypeStack.Push(TokenType.VectorStart);
				}
				else if (token.Type == TokenType.VectorEnd)
				{
					if (_sequenceStack.Count == 1) throw new ParsingException("Unexpected token: " + token.Text);
					if (_sequenceTypeStack.Peek().MatchingBraceType() != token.Type) throw new ParsingException("Unexpected token: " + token.Text);
					var completedList = new LispVector(_sequenceStack.Pop());
					_sequenceStack.Peek().Add(completedList);
					_sequenceTypeStack.Pop();
				}
				else if (token.Type == TokenType.MapStart)
				{
					_sequenceStack.Push(new List<object>());
					_sequenceTypeStack.Push(TokenType.MapStart);
				}
				else if (token.Type == TokenType.MapEnd)
				{
					if (_sequenceStack.Count == 1) throw new ParsingException("Unexpected token: " + token.Text);
					if (_sequenceTypeStack.Peek().MatchingBraceType() != token.Type) throw new ParsingException("Unexpected token: " + token.Text);
					if (_sequenceStack.Peek().Count % 2 != 0) throw new InvalidMapElementCountException();
					var mapAsList = _sequenceStack.Pop();

					_sequenceStack.Peek().Add(Map.ParseMap(mapAsList));
					_sequenceTypeStack.Pop();
				}
				else if (token.Type == TokenType.Null)
				{
					_sequenceStack.Peek().Add(null);
				}
				else if (token.Type == TokenType.String)
				{
					_sequenceStack.Peek().Add(token.Text.Trim(new[] {'\"'}));
				}
				else if (token.Type == TokenType.Integer)
				{
					_sequenceStack.Peek().Add(int.Parse(token.Text));
				}
				else if (token.Type == TokenType.Double)
				{
					_sequenceStack.Peek().Add(double.Parse(token.Text));
				}
				else if (token.Type == TokenType.Boolean)
				{
					_sequenceStack.Peek().Add(bool.Parse(token.Text));
				}
				else if (token.Type == TokenType.HexNumber)
				{
					var num = Int32.Parse(token.Text.Replace("0x", ""), NumberStyles.HexNumber);
					_sequenceStack.Peek().Add((int) num);
				}
				else if (token.Type == TokenType.Symbol)
				{
					_sequenceStack.Peek().Add(new Symbol(token.Text));
				}
				else if (token.Type == TokenType.VariableArity)
				{
					_sequenceStack.Peek().Add(new Symbol(token.Text));
				}
				else if (token.Type == TokenType.Unknown)
				{
					throw new ParsingException("Unknown token: " + token.Text + " at " + token.StartIndex);
				}

				token = _lexer.Next();
			}

			if (_sequenceStack.Count > 1) throw new ParsingException("Expected " + _sequenceTypeStack.Peek().MatchingBraceType().ToString());
			return _sequenceStack.Peek();
		}
	}
}