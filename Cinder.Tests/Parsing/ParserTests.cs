using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cinder.Compiler.IO;
using Cinder.Compiler.Parsing;
using Cinder.Compiler.Parsing.Data;
using Cinder.Compiler.Parsing.Lexical;
using Cinder.Runtime;
using Cinder.Runtime.Sequences;
using NUnit.Framework;
using Cinder.Compiler;
using Cinder.Compiler.Expressions;

namespace Cinder.Tests.Parsing
{
	[TestFixture]
	public class ParserTests
	{
		[Test]
		public void Parse_CreatesMapWithOneEntryWithListAsKey()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("{(+ 1 1) 2}"))));
			List<object> result = parser.Parse();
			var map = result[0] as Map;
			var kvp = (KeyValuePair<object, object>) map.First();
			Assert.AreEqual(typeof(LispList), kvp.Key.GetType ());
		}

		[Test]
		public void Parse_CreatesMapWithOneEntry()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("{\"key\" 1}"))));
			List<object> result = parser.Parse();
			var map = result[0] as Map;
			Assert.AreEqual(1, map.Count());
		}

		[Test]
		public void Parse_CreatesEmptyMap()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("{}"))));
			List<object> result = parser.Parse();
			var map = result[0] as Map;
			Assert.AreEqual(0, map.Count());
		}

		[Test]
		[ExpectedException(typeof(InvalidMapElementCountException))]
		public void Parse_ThrowsExceptionWhenMapDoesNotContainEvenNumberOfElements()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("{\"asdf\"}"))));
			parser.Parse();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Parse_ThrowsExceptionForMissingEndMapToken()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("{\"asdf\""))));
			parser.Parse();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Parse_ThrowsExceptionForEndMapTokenWithoutStart()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("}"))));
			parser.Parse();
		}

		[Test]
		public void Parse_ShouldReturnAVectorWithTwoNumbers()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("[1 2]"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			var vector = result[0] as LispVector;
			Assert.AreEqual(1, vector.First());
			Assert.AreEqual(2, vector.Rest().First());
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Parse_ShouldThrowExceptionWhenEncounteringUnexpectedClosingVectorToken()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("1 2]"))));
			parser.Parse();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Parse_ShouldThrowExceptionWhenEncounteringListClosingTokenInsteadOfVectorClosingToken()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("[1 2)"))));
			parser.Parse();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Parse_ShouldThrowExceptionWhenEncounteringUnclosedVector()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("[1 2"))));
			parser.Parse();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Parse_ShouldThrowExceptionWhenEncounteringEndListTokenWithoutAssociatedOpenList()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader(")"))));
			parser.Parse();
		}

		[Test]
		public void Parse_ShouldReturnAListOfLengthTwoWhenInputHasTwoTokens()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("test"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(typeof (Symbol), result[0].GetType());
		}

		[Test]
		public void Parse_ShouldReturnAListOfLengthOneWhenInputHasOnlyOneToken()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("test1 test2"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual(typeof (Symbol), result[0].GetType());
			Assert.AreEqual(typeof (Symbol), result[1].GetType());
		}

		[Test]
		public void Parse_ShouldReturnALispListWhenInputHasOnlyOneLispList()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("()"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(typeof (LispList), result[0].GetType());
		}

		[Test]
		public void Parse_ShouldReturnALispListWithOneSymbolWhenInputHasOnlyOneLispListWithOneSymbol()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("(do)"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(typeof (LispList), result[0].GetType());
			LispList list = (LispList) result[0];
			Assert.AreEqual(1, list.Count());
		}

		[Test]
		public void Parse_ShouldReturnALispListWithOneSymbolAndOneString()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("(println \"Test\")"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(typeof (LispList), result[0].GetType());
			LispList list = (LispList) result[0];
			Assert.AreEqual(2, list.Count());
			Assert.AreEqual(typeof (Symbol), list.First().GetType());
			Assert.AreEqual(typeof (string), list.Rest().First().GetType());
		}

		[Test]
		public void Parse_ShouldReturnASymbolFollowedByALispList()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("println ()"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual(typeof (Symbol), result[0].GetType());
			Assert.AreEqual(typeof (LispList), result[1].GetType());
		}

		[Test]
		public void Parse_ShouldReturnTwoLispLists()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("() ()"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual(typeof (LispList), result[0].GetType());
			Assert.AreEqual(typeof (LispList), result[1].GetType());
		}

		[Test]
		public void Parse_ShouldReturnANumber()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("123"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(typeof (int), result[0].GetType());
		}

		[Test]
		public void Parse_ShouldReturnANumberFromHexNumber()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("0x1"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(typeof(int), result[0].GetType());
		}

		[Test]
		public void Parse_ShouldReturnATrueBoolean()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("true"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(true, result[0]);
		}

		[Test]
		public void Parse_ShouldReturnAFalseBoolean()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("false"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(false, result[0]);
		}

		[Test]
		public void Parse_ShouldReturnANull()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("null"))));
			List<object> result = parser.Parse();
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(null, result[0]);
		}

		[Test]
		public void Parse_ShouldReturnNestedLists()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("(println (str \"test\"))"))));
			List<object> result = parser.Parse();
			LispList list = (LispList) result[0];
			Assert.AreEqual(typeof (LispList), list.Rest().First().GetType());
		}

		[Test]
		[ExpectedException(typeof (ParsingException))]
		public void Parse_ShouldThrowExceptionForIncompleteList()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("(println"))));
			parser.Parse();
		}

		[Test]
		public void Parse_ShouldRemoveSurroundingQuotesFromString()
		{
			Parser parser = new Parser(new Lexer(new PushBackCharacterStream(new StringReader("\"asdf\""))));
			List<object> result = parser.Parse();
			Assert.AreEqual("asdf", (string) result[0]);
		}
	}
}