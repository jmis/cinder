using System.IO;
using Cinder.Compiler.IO;
using Cinder.Compiler.Parsing.Lexical;
using NUnit.Framework;

namespace Cinder.Tests.Parsing
{
	[TestFixture]
	public class LexerTests
	{
		[Test]
		public void Next_ShouldReturnNullWhenAtEndOfStream()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("")));
			Assert.IsNull(lexer.Next());
		}

		[Test]
		public void Next_ShouldReturnIntegerTokenTypeWhenInputIsNumber()
		{
			var stream = new PushBackCharacterStream(new StringReader("123"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Integer, token.Type);
			Assert.AreEqual("123", token.Text);
			Assert.IsFalse(stream.HasMore);
		}

		[Test]
		public void Next_ShouldReturnDouble()
		{
			var stream = new PushBackCharacterStream(new StringReader("123.321"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Double, token.Type);
			Assert.AreEqual("123.321", token.Text);
		}

		[Test]
		public void Next_ShouldReturnHexNumber()
		{
			var stream = new PushBackCharacterStream(new StringReader("0x123A"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.HexNumber, token.Type);
			Assert.AreEqual("0x123A", token.Text);
			Assert.IsFalse(stream.HasMore);
		}

		[Test]
		public void Next_ReturnsUnknownTokenInputStartingWithNumberButEndingWithLetters()
		{
			var stream = new PushBackCharacterStream(new StringReader("123asdf"));
			Lexer lexer = new Lexer(stream);
			Assert.AreEqual(TokenType.Unknown, lexer.Next().Type);
		}

		[Test]
		public void Next_ShouldReturnListStartTokenTypeWhenInputIsAnOpenParen()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("(")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.ListStart, token.Type);
			Assert.AreEqual("(", token.Text);
		}

		[Test]
		public void Next_ShouldReturnListEndTokenTypeWhenInputIsAClosedParen()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader(")")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.ListEnd, token.Type);
			Assert.AreEqual(")", token.Text);
		}

		[Test]
		public void Next_ShouldReturnVectorStartTokenTypeWhenInputIsAnOpenBracket()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("[")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.VectorStart, token.Type);
			Assert.AreEqual("[", token.Text);
		}

		[Test]
		public void Next_ShouldReturnVectorEndTokenTypeWhenInputIsAClosedBracket()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("]")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.VectorEnd, token.Type);
			Assert.AreEqual("]", token.Text);
		}

		[Test]
		public void Next_ShouldReturnMapStartTokenTypeWhenInputIsAnOpenCurlyBrace()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("{")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.MapStart, token.Type);
			Assert.AreEqual("{", token.Text);
		}

		[Test]
		public void Next_ShouldReturnMapEndTokenTypeWhenInputIsAClosedCurlyBrace()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("}")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.MapEnd, token.Type);
			Assert.AreEqual("}", token.Text);
		}

		[Test]
		public void Next_ShouldReturnStringForProperlyTerminatingString()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("\"asdf\"")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.String, token.Type);
			Assert.AreEqual("\"asdf\"", token.Text);
		}

		[Test]
		public void Next_ShouldReturnStringForRunOnString()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("\"asdfasdf")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.String, token.Type);
			Assert.AreEqual("\"asdfasdf", token.Text);
		}

		[Test]
		public void Next_ShouldReturnStringThatDoesNotTerminateOnBackslashQuote()
		{
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader("\"asdf\\\"asdf\"")));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.String, token.Type);
			Assert.AreEqual("\"asdf\\\"asdf\"", token.Text);
		}

		[Test]
		public void Next_ShouldReturnWhitespaceForTabsSpacesCommasAndReturnCharacters()
		{
			string input = "  \t \r\n , ";
			Lexer lexer = new Lexer(new PushBackCharacterStream(new StringReader(input)));
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Whitespace, token.Type);
			Assert.AreEqual(input, token.Text);
		}

		[Test]
		public void Next_ShouldReturnIntegerFollowByWhitespaceAndAString()
		{
			var stream = new PushBackCharacterStream(new StringReader("123 \"asdf\""));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Integer, token.Type);
			Assert.AreEqual("123", token.Text);
			Assert.AreEqual(0, token.StartIndex);

			token = lexer.Next();
			Assert.AreEqual(TokenType.Whitespace, token.Type);
			Assert.AreEqual(" ", token.Text);
			Assert.AreEqual(3, token.StartIndex);

			token = lexer.Next();
			Assert.AreEqual(TokenType.String, token.Type);
			Assert.AreEqual("\"asdf\"", token.Text);
			Assert.AreEqual(4, token.StartIndex);
		}

		[Test]
		public void Next_ShouldReturnCommentWithTrailingWhitespace()
		{
			var stream = new PushBackCharacterStream(new StringReader("; test text  \r\n"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Comment, token.Type);
			Assert.AreEqual("; test text", token.Text);

			token = lexer.Next();
			Assert.AreEqual(TokenType.Whitespace, token.Type);
			Assert.AreEqual("  \r\n", token.Text);
		}

		[Test]
		public void Next_ShouldReturnCommentThatExtendsToEndOfInput()
		{
			var stream = new PushBackCharacterStream(new StringReader("; test"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Comment, token.Type);
			Assert.AreEqual("; test", token.Text);
		}

		[Test]
		public void Next_ShouldReturnCommentToEndOfLineOnly()
		{
			var stream = new PushBackCharacterStream(new StringReader("; test\r\n123"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Comment, token.Type);
			Assert.AreEqual("; test", token.Text);
		}

		[Test]
		public void Next_ShouldReturnSymbol()
		{
			var stream = new PushBackCharacterStream(new StringReader("test"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Symbol, token.Type);
			Assert.AreEqual("test", token.Text);
		}

		[Test]
		public void Next_ShouldReturnSymbolWhenItHasADot()
		{
			var stream = new PushBackCharacterStream(new StringReader("namespace.test"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Symbol, token.Type);
			Assert.AreEqual("namespace.test", token.Text);
		}

		[Test]
		public void Next_ShouldReturnSymbolImmediatelyFollowedByComment()
		{
			var stream = new PushBackCharacterStream(new StringReader("test;comment"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Symbol, token.Type);
			Assert.AreEqual("test", token.Text);

			token = lexer.Next();
			Assert.AreEqual(TokenType.Comment, token.Type);
			Assert.AreEqual(";comment", token.Text);
		}

		[Test]
		public void Next_ShouldReturnTwoSymbolsSeparatedByWhitespace()
		{
			var stream = new PushBackCharacterStream(new StringReader("symbol1 symbol2"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Symbol, token.Type);
			Assert.AreEqual("symbol1", token.Text);

			token = lexer.Next();
			Assert.AreEqual(TokenType.Whitespace, token.Type);

			token = lexer.Next();
			Assert.AreEqual(TokenType.Symbol, token.Type);
			Assert.AreEqual("symbol2", token.Text);
		}

		[Test]
		public void Next_ShouldReturnKeyword()
		{
			var stream = new PushBackCharacterStream(new StringReader(":asdf"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Keyword, token.Type);
			Assert.AreEqual(":asdf", token.Text);
		}

		[Test]
		public void Next_ShouldReturnKeywordWithNoName()
		{
			var stream = new PushBackCharacterStream(new StringReader(":"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Keyword, token.Type);
			Assert.AreEqual(":", token.Text);
		}

		[Test]
		public void Next_ShouldReturnKeywordFollowByListStart()
		{
			var stream = new PushBackCharacterStream(new StringReader(":asdf("));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Keyword, token.Type);
			Assert.AreEqual(":asdf", token.Text);
			Assert.AreEqual(TokenType.ListStart, lexer.Next().Type);
		}

		[Test]
		public void Next_ShouldReturnBooleanWhenTrueIsInput()
		{
			var stream = new PushBackCharacterStream(new StringReader("true"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Boolean, token.Type);
			Assert.AreEqual("true", token.Text);
			Assert.IsFalse(stream.HasMore);
		}

		[Test]
		public void Next_ShouldReturnBooleanWhenFalseIsInput()
		{
			var stream = new PushBackCharacterStream(new StringReader("false"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Boolean, token.Type);
			Assert.AreEqual("false", token.Text);
			Assert.IsFalse(stream.HasMore);
		}

		[Test]
		public void Next_ShouldReturnNull()
		{
			var stream = new PushBackCharacterStream(new StringReader("null"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Null, token.Type);
			Assert.AreEqual("null", token.Text);
			Assert.IsFalse(stream.HasMore);
		}

		[Test]
		public void Next_ShouldStopParsingSymbolWhenDoubleQuoteFound()
		{
			var stream = new PushBackCharacterStream(new StringReader("asdf\"str\""));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.Symbol, token.Type);
			Assert.AreEqual("asdf", token.Text);

			token = lexer.Next();
			Assert.AreEqual(TokenType.String, token.Type);
			Assert.AreEqual("\"str\"", token.Text);
		}

		[Test]
		public void Next_ShouldReturnVariableArityToken()
		{
			var stream = new PushBackCharacterStream(new StringReader("&"));
			Lexer lexer = new Lexer(stream);
			Token token = lexer.Next();
			Assert.AreEqual(TokenType.VariableArity, token.Type);
			Assert.AreEqual("&", token.Text);
		}
	}
}