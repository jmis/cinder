using System;
using System.Collections;
using System.Text;
using Cinder.Compiler;
using Cinder.Compiler.Parsing;
using Cinder.Runtime;
using Cinder.Runtime.Sequences;
using NUnit.Framework;
using System.Collections.Generic;

namespace Cinder.Tests.Compiler
{
	[TestFixture]
	public class CompilationTests
	{
		[TearDown]
		public void Cleanup()
		{
			//Compilation.DumpNamespaceToFile();
		}

		[Test]
		public void QuotedMap()
		{
			Map map = Compilation.CreateFunctionFromSource("(quote {(+ 1 1) 2})").Apply() as Map;
			KeyValuePair<object, object> kvp = (KeyValuePair<object, object>) map.First();
			Assert.AreEqual(typeof(LispList), kvp.Key.GetType());
			Assert.AreEqual(2, kvp.Value);
		}

		[Test]
		public void Map_CreateWithOneEntry()
		{
			Map map = Compilation.CreateFunctionFromSource("{1 2}").Apply() as Map;
			Assert.IsNotNull(map);
			KeyValuePair<object, object> kvp = (KeyValuePair<object, object>) map.First();
			Assert.AreEqual(1, kvp.Key);
			Assert.AreEqual(2, kvp.Value);
		}

		[Test]
		public void New_NoArgs()
		{
			var builder = Compilation.CreateFunctionFromSource("(new System.Text.StringBuilder)").Apply();
			Assert.AreEqual(typeof (StringBuilder), builder.GetType());
		}

		[Test]
		public void New_OneIntegerArg()
		{
			var builder = Compilation.CreateFunctionFromSource("(new System.Collections.ArrayList 1)").Apply();
			Assert.AreEqual(typeof (ArrayList), builder.GetType());
		}

		[Test]
		public void New_OneStringArg()
		{
			var builder =
				Compilation.CreateFunctionFromSource("(new System.Text.StringBuilder \"Test\")").Apply() as StringBuilder;
			Assert.AreEqual("Test", builder.ToString());
		}

		[Test]
		public void New_MoreThanOneArg()
		{
			var builder = Compilation.CreateFunctionFromSource("(new System.Text.StringBuilder 1 5)").Apply() as StringBuilder;
			Assert.AreEqual(1, builder.Capacity);
			Assert.AreEqual(5, builder.MaxCapacity);
		}

		[Test]
		public void Def_Primitive()
		{
			Compilation.CreateFunctionFromSource("(def test-var 1)").Apply();
			Assert.AreEqual(1, Namespace.Get("test-var").Current);
		}

		[Test]
		public void VarArgs_OneValue()
		{
			var function = Compilation.CreateFunctionFromSource("(fn [& elements] elements)").Apply() as IFunction;
			var result = function.Apply(1) as Sequence;
			Assert.AreEqual(1, result.First());
		}

		[Test]
		public void Invocation_OneArg()
		{
			var result = Compilation.CreateFunctionFromSource("((fn [x] x) 10)").Apply();
			Assert.AreEqual(10, result);
		}

		[Test]
		public void Invocation_VarArgs()
		{
			var result = Compilation.CreateFunctionFromSource("((fn [& x] x) 1)").Apply() as Sequence;
			Assert.AreEqual(1, result.First());
		}

		[Test]
		public void CapturedVariables_OneArgument()
		{
			var result = Compilation.CreateFunctionFromSource("(((fn [x] (fn [] x)) 1))").Apply();
			Assert.AreEqual(1, result);
		}

		[Test]
		public void CapturedVariables_TwoArgument()
		{
			var result = Compilation.CreateFunctionFromSource("(((fn [x y] (fn [] y)) 1 2))").Apply();
			Assert.AreEqual(2, result);
		}

		[Test]
		public void CapturedVariables_OutterScopeLocal()
		{
			var func = Compilation.CreateFunctionFromSource("(def-local x 1 (fn [] x))").Apply() as IFunction;
			Assert.AreEqual(1, func.Apply());
		}

		[Test]
		public void CapturedVariables_OutterScopeLocalAndArgument()
		{
			var outterFunc = Compilation.CreateFunctionFromSource("(fn [y] (def-local x 1 (fn [] y)))").Apply() as IFunction;
			var innerFunc = outterFunc.Apply(3) as IFunction;
			Assert.AreEqual(3, innerFunc.Apply());
		}

		[Test]
		public void Conditional_True()
		{
			Assert.AreEqual(1, Compilation.CreateFunctionFromSource("(if true 1 2)").Apply());
		}

		[Test]
		public void Conditional_False()
		{
			Assert.AreEqual(2, Compilation.CreateFunctionFromSource("(if false 1 2)").Apply());
		}

		[Test]
		public void InstanceOf_Equal()
		{
			var result =
				Compilation.CreateFunctionFromSource("(instance-of? System.Text.StringBuilder (new System.Text.StringBuilder))").
					Apply();
			Assert.IsNotNull(result);
			Assert.AreEqual(typeof (StringBuilder), result.GetType());
		}

		[Test]
		public void InstanceOf_NotEqual()
		{
			var result =
				Compilation.CreateFunctionFromSource("(instance-of? System.Collections.ArrayList (new System.Text.StringBuilder))").
					Apply();
			Assert.IsNull(result);
		}

		[Test]
		public void DefLocal()
		{
			Compilation.CreateFunctionFromSource("(def-local x 1 x)").Apply();
		}

		[Test]
		public void ObjectMethodInvocation()
		{
			var builder = Compilation.CreateFunctionFromSource("(. (new System.Text.StringBuilder) Append \"Test\")").Apply();
			Assert.AreEqual("Test", builder.ToString());
		}

		[Test]
		public void Quote()
		{
			var result = Compilation.CreateFunctionFromSource("(quote 33)").Apply();
			Assert.AreEqual(33, result);
		}

		[Test]
		public void StaticMethodInvocation()
		{
			var result = Compilation.CreateFunctionFromSource("(. System.Int32 Parse \"33\")").Apply();
			Assert.AreEqual(33, result);
		}

		[Test]
		public void Var()
		{
			Namespace.Create("vartest").Push(100);
			Assert.AreEqual(100, Compilation.CreateFunctionFromSource("vartest").Apply());
		}

		[Test]
		[ExpectedException(typeof(Exception))]
		public void Throw()
		{
			Compilation.CreateFunctionFromSource("(throw (new System.Exception))").Apply();
		}

		[Test]
		public void Try_ReturnsValueFromTryBlockWhenNoExceptionOccurs()
		{
			Assert.AreEqual(1, Compilation.CreateFunctionFromSource("(try 1 (catch System.Exception e 50))").Apply());
		}

		[Test]
		public void Try_ReturnsValueFromCatchBlockWhenExceptionOccurs()
		{
			Assert.AreEqual(50, Compilation.CreateFunctionFromSource("(try (throw (new System.Exception)) (catch System.Exception e 50))").Apply());
		}

		[Test]
		public void Try_ExecutesFinallyBlockWhenNoExceptionOccurs()
		{
			Namespace.Create("x").Push(0);
			Assert.AreEqual(1, Compilation.CreateFunctionFromSource("(try 1 (catch System.Exception e 50) (finally (def x 30)))").Apply());
			Assert.AreEqual(30, Namespace.Get("x").Current);
		}

		[Test]
		public void Try_ExecutesFinallyBlockWhenNoExceptionOccursAndThereIsNoCatchBlock()
		{
			Namespace.Create("x").Push(0);
			Assert.AreEqual(1, Compilation.CreateFunctionFromSource("(try 1 (finally (def x 30)))").Apply());
			Assert.AreEqual(30, Namespace.Get("x").Current);
		}

		[Test]
		public void Try_ExecutesExecutesTheAssociatedCatchBlockWhenExceptionIsThrown()
		{
			Assert.AreEqual(9, Compilation.CreateFunctionFromSource("(try (throw (new System.ArithmeticException)) (catch System.ArithmeticException e 9) (catch System.Exception e 50))").Apply());
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void Try_UncaughtExceptionsBubbleUp()
		{
			Compilation.CreateFunctionFromSource("(try (throw (new System.ArgumentException)) (catch System.ArithmeticException e 50))").Apply();
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void Try_UncaughtExceptionsBubbleUpWhenNoAssociatedCatchBlockWithFinallyBlock()
		{
			Compilation.CreateFunctionFromSource("(try (throw (new System.ArgumentException)) (catch System.ArithmeticException e 50) (finally 10))").Apply();
		}

		[Test]
		[ExpectedException(typeof(Exception))]
		public void Try_ExceptionsThrownInsideOfCatchBlockBubbleUp()
		{
			Compilation.CreateFunctionFromSource("(try (throw (new System.ArithmeticException)) (catch System.ArithmeticException e (throw (new System.Exception))))").Apply();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Try_TryBlockWithoutCatchOrFinallyThrowsParsingException()
		{
			Compilation.CreateFunctionFromSource("(try 1)").Apply();
		}

		[Test]
		[ExpectedException(typeof(ParsingException))]
		public void Try_ConflictingExceptionIdentifierThrowsException()
		{
			Compilation.CreateFunctionFromSource("(def-local e 5 (try 1 (catch System.Exception e 2)))").Apply();
		}

		[Test]
		public void Try_ExceptionIsBoundToIdentifier()
		{
			Assert.AreEqual(typeof(ArgumentException), Compilation.CreateFunctionFromSource("(try (throw (new System.ArgumentException)) (catch System.ArgumentException e e))").Apply().GetType());
		}

		[Test]
		public void Try_TreatedAsExpression()
		{
			Assert.AreEqual(2, Compilation.CreateFunctionFromSource("(. Cinder.Runtime.Library.Numbers Add 1 (try 1 (catch System.Exception e)))").Apply());
		}

		[Test]
		public void VarArgs_VariableArityParameterCanBePrecededBySingleParameter()
		{
			Assert.AreEqual(1, Compilation.CreateFunctionFromSource("((fn [x & y] x) 1 2)").Apply());
			Assert.AreEqual(2, (Compilation.CreateFunctionFromSource("((fn [x & y] y) 1 2)").Apply() as Sequence).First());
		}

		[Test]
		public void VarArgs_VariableArityParameterCanBePrecededByTwoParameters()
		{
			Assert.AreEqual(2, Compilation.CreateFunctionFromSource("((fn [x y & z] y) 1 2)").Apply());
		}

		[Test]
		public void VarArgs_PassesEmptySequenceToFunctionWhenNoVariableArgumentsProvided()
		{
			Assert.IsTrue((Compilation.CreateFunctionFromSource("((fn [x & y] y) 1)").Apply() as Sequence).IsEmpty());
		}
	}
}