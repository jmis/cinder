using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cinder.Runtime.Interop.Invocation;
using Cinder.Runtime.Sequences;
using NUnit.Framework;

namespace Cinder.Tests.Runtime.Interop.Invocation
{
	[TestFixture]
	public class DelegateFactoryTests
	{
		[Test]
		public void Test()
		{
			var method = DelegateFactory.CreateConstructor(typeof (LispList).GetConstructors()[3]);
			var builder = method.Invoke(new[] { new LispList() });
		}
	}
}
