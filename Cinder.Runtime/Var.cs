using System.Collections.Generic;

namespace Cinder.Runtime
{
	public class Var
	{
		private readonly string _name;
		private readonly Stack<object> _bindings;
		private object _top;

		public Var(string name)
		{
			_name = name;
			_bindings = new Stack<object>();
		}

		public Var(string name, object val)
		{
			_name = name;
			_bindings = new Stack<object>();
			Push(val);
		}

		public string Name
		{
			get { return _name; }
		}

		public void Push(object o)
		{
			_top = o;
			_bindings.Push(o);
		}

		public void Pop()
		{
			_bindings.Pop();
			_top = _bindings.Peek();
		}

		public object Current
		{
			get { return _top; }
		}

		public bool IsBound
		{
			get { return _bindings.Count > 0; }
		}
	}
}