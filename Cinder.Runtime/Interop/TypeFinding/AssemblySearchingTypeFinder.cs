using System;

namespace Cinder.Runtime.Interop.TypeFinding
{
	public class AssemblySearchingTypeFinder : ITypeFinder
	{
		public Type FindType(string fullTypeName)
		{
			var type = Type.GetType(fullTypeName);
			if (type != null) return type;

			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				var t2 = assembly.GetType(fullTypeName);
				if (t2 != null) return t2;
			}

			return null;
		}
	}
}