using System;
using System.Collections.Generic;

namespace Cinder.Runtime.Interop.TypeFinding
{
	public class CachingTypeFinder : ITypeFinder
	{
		private readonly ITypeFinder _internalTypeFinder;
		private readonly Dictionary<string, Type> _cache;

		public CachingTypeFinder(ITypeFinder internalTypeFinder, Dictionary<string, Type> cache)
		{
			_internalTypeFinder = internalTypeFinder;
			_cache = cache;
		}

		public Type FindType(string fullTypeName)
		{
			if (_cache.ContainsKey(fullTypeName)) return _cache[fullTypeName];
			var type = _internalTypeFinder.FindType(fullTypeName);
			if (type == null) return null;
			_cache[fullTypeName] = type;
			return _cache[fullTypeName];
		}
	}
}