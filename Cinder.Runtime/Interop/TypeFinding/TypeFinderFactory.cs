using System;
using System.Collections.Generic;

namespace Cinder.Runtime.Interop.TypeFinding
{
	public class TypeFinderFactory
	{
		public static readonly Dictionary<string, Type> TYPE_CACHE = new Dictionary<string, Type>();

		public static ITypeFinder CreateCachingTypeFinder()
		{
			return new CachingTypeFinder(new AssemblySearchingTypeFinder(), TYPE_CACHE);
		}
	}
}