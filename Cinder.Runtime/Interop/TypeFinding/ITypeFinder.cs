using System;

namespace Cinder.Runtime.Interop.TypeFinding
{
	public interface ITypeFinder
	{
		Type FindType(string fullTypeName);
	}
}