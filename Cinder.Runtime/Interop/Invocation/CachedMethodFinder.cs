using System;
using System.Collections.Generic;
using System.Text;

namespace Cinder.Runtime.Interop.Invocation
{
	public class CachedMethodFinder : IMethodFinder
	{
		private readonly IMethodFinder _internalMethodFinder;
		private readonly Dictionary<string, DynamicMethodDelegate> _methodCache;
		private readonly Dictionary<string, DynamicConstructorDelegate> _constructorCache;

		public CachedMethodFinder(IMethodFinder internalMethodFinder, Dictionary<string, DynamicMethodDelegate> methodCache, Dictionary<string, DynamicConstructorDelegate> constructorCache)
		{
			_internalMethodFinder = internalMethodFinder;
			_methodCache = methodCache;
			_constructorCache = constructorCache;
		}

		public DynamicMethodDelegate FindMethod(Type type, string methodName, Type[] signature)
		{
			var methodKeyBuilder = new StringBuilder();
			methodKeyBuilder.Append(type.Name).Append(methodName);
			foreach (var parameterType in signature) methodKeyBuilder.Append(parameterType.Name);
			var methodKey = methodKeyBuilder.ToString();
			if (_methodCache.ContainsKey(methodKey)) return _methodCache[methodKey];
			var foundMethod = _internalMethodFinder.FindMethod(type, methodName, signature);
			_methodCache[methodKey] = foundMethod;
			return foundMethod;
		}

		public DynamicConstructorDelegate FindConstructor(Type type, Type[] signature)
		{
			var constructorKeyBuilder = new StringBuilder();
			constructorKeyBuilder.Append(type.Name);
			foreach (var parameterType in signature) constructorKeyBuilder.Append(parameterType.Name);
			var constructorKey = constructorKeyBuilder.ToString();
			if (_constructorCache.ContainsKey(constructorKey)) return _constructorCache[constructorKey];
			var foundConstructor = _internalMethodFinder.FindConstructor(type, signature);
			_constructorCache[constructorKey] = foundConstructor;
			return foundConstructor;
		}
	}
}