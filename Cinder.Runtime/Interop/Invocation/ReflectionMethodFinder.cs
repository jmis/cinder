using System;

namespace Cinder.Runtime.Interop.Invocation
{
	public class ReflectionMethodFinder : IMethodFinder
	{
		public DynamicMethodDelegate FindMethod(Type type, string methodName, Type[] signature)
		{
			var methodInfo = type.GetMethod(methodName, signature);
			if (methodInfo != null) return DelegateFactory.Create(methodInfo);
			return null;
		}

		public DynamicConstructorDelegate FindConstructor(Type type, Type[] signature)
		{
			var constructorInfo = type.GetConstructor(signature);
			if (constructorInfo != null) return DelegateFactory.CreateConstructor(constructorInfo);
			return null;
		}
	}
}