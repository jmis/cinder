/*
 *  DynamicMethod Delegates Demo
 * 
 *  Copyright (C) 2005 
 *      Alessandro Febretti <mailto:febret@gmail.com>
 *      SharpFactory
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Security;

namespace Cinder.Runtime.Interop.Invocation
{
	public delegate object DynamicMethodDelegate(object target, object[] args);

	public delegate object DynamicConstructorDelegate(object[] args);

	[SecuritySafeCritical]
	public class DelegateFactory
	{
		public static DynamicConstructorDelegate CreateConstructor(ConstructorInfo constructor)
		{
			Type[] argTypes = {typeof (object[])};
			var dynamicMethod = new DynamicMethod("Create", constructor.DeclaringType, argTypes, typeof (DelegateFactory));
			var il = dynamicMethod.GetILGenerator();
			var parms = constructor.GetParameters();
			var numparams = parms.Length;
			var argsOK = il.DefineLabel();

			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldlen);
			il.Emit(OpCodes.Ldc_I4, numparams);
			il.Emit(OpCodes.Beq, argsOK);
			il.Emit(OpCodes.Newobj, typeof (TargetParameterCountException).GetConstructor(Type.EmptyTypes));
			il.Emit(OpCodes.Throw);
			il.MarkLabel(argsOK);

			var i = 0;

			while (i < numparams)
			{
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldc_I4, i);
				il.Emit(OpCodes.Ldelem_Ref);

				var parmType = parms[i].ParameterType;
				if (parmType.IsValueType) il.Emit(OpCodes.Unbox_Any, parmType);
				i++;
			}

			il.Emit(OpCodes.Newobj, constructor);
			il.Emit(OpCodes.Ret);
			return (DynamicConstructorDelegate) dynamicMethod.CreateDelegate(typeof (DynamicConstructorDelegate));
		}

		public static DynamicMethodDelegate Create(MethodInfo method)
		{
			var parms = method.GetParameters();
			var numparams = parms.Length;

			Type[] _argTypes = {typeof (object), typeof (object[])};

			// Create dynamic method and obtain its IL generator to
			// inject code.
			var dynam = new DynamicMethod(method.MethodHandle.Value.ToInt64().ToString(), typeof (object), _argTypes, typeof (DelegateFactory));
			var il = dynam.GetILGenerator();

			// Define a label for succesfull argument count checking.
			var argsOK = il.DefineLabel();

			// Check input argument count.
			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Ldlen);
			il.Emit(OpCodes.Ldc_I4, numparams);
			il.Emit(OpCodes.Beq, argsOK);

			// Argument count was wrong, throw TargetParameterCountException.
			il.Emit(OpCodes.Newobj, typeof (TargetParameterCountException).GetConstructor(Type.EmptyTypes));
			il.Emit(OpCodes.Throw);

			// Mark IL with argsOK label.
			il.MarkLabel(argsOK);

			// If method isn't static push target instance on top
			// of stack.

			if (!method.IsStatic)
			{
				// Argument 0 of dynamic method is target instance.
				il.Emit(OpCodes.Ldarg_0);

				if (method.DeclaringType.IsValueType)
				{
					il.Emit(OpCodes.Unbox, method.DeclaringType);
				}
			}

			// Lay out args array onto stack.
			var i = 0;
			while (i < numparams)
			{
				// Push args array reference onto the stack, followed
				// by the current argument index (i). The Ldelem_Ref opcode
				// will resolve them to args[i].

				// Argument 1 of dynamic method is argument array.
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldc_I4, i);
				il.Emit(OpCodes.Ldelem_Ref);

				// If parameter [i] is a value type perform an unboxing.
				var parmType = parms[i].ParameterType;
				if (parmType.IsValueType)
				{
					il.Emit(OpCodes.Unbox_Any, parmType);
				}

				i++;
			}

			if (method.IsFinal || !method.IsVirtual)
			{
				il.Emit(OpCodes.Call, method);
			}
			else
			{
				il.Emit(OpCodes.Callvirt, method);
			}

			if (method.ReturnType != typeof (void))
			{
				// If result is of value type it needs to be boxed
				if (method.ReturnType.IsValueType)
				{
					il.Emit(OpCodes.Box, method.ReturnType);
				}
			}
			else
			{
				il.Emit(OpCodes.Ldnull);
			}

			il.Emit(OpCodes.Ret);

			return (DynamicMethodDelegate) dynam.CreateDelegate(typeof (DynamicMethodDelegate));
		}
	}
}