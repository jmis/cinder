using System.Collections.Generic;

namespace Cinder.Runtime.Interop.Invocation
{
	public class MethodFinderFactory
	{
		public static readonly Dictionary<string, DynamicMethodDelegate> METHOD_CACHE = new Dictionary<string, DynamicMethodDelegate>();
		public static readonly Dictionary<string, DynamicConstructorDelegate> CONSTRUCTOR_CACHE = new Dictionary<string, DynamicConstructorDelegate>();
		public static IMethodFinder CACHED_METHODFINDER = new CachedMethodFinder(new ReflectionMethodFinder(), METHOD_CACHE, CONSTRUCTOR_CACHE);

		public static IMethodFinder CreateMethodFinder()
		{
			return CACHED_METHODFINDER;
		}
	}
}