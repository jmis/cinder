using System;

namespace Cinder.Runtime.Interop.Invocation
{
	public interface IMethodFinder
	{
		DynamicMethodDelegate FindMethod(Type type, string methodName, Type[] signature);
		DynamicConstructorDelegate FindConstructor(Type type, Type[] signature);
	}
}