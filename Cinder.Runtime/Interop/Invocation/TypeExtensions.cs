using System;

namespace Cinder.Runtime.Interop.Invocation
{
	public static class TypeExtensions
	{
		public static Type[] GetTypes(this object[] objects)
		{
			var types = new Type[objects.Length];

			for (var i = 0; i < objects.Length; i++)
			{
				if (objects[i] == null) types[i] = typeof (void);
				else types[i] = objects[i].GetType();
			}

			return types;
		}
	}
}