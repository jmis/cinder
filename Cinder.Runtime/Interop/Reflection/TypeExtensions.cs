using System;
using System.Collections.Generic;
using System.Reflection;

namespace Cinder.Runtime.Interop.Reflection
{
	public static class TypeExtensions
	{
		public static ConstructorInfo FindMatchingConstructor(this Type t, List<Type> signature)
		{
			foreach (var c in t.GetConstructors())
				if (TypesMatch(c.GetParameters(), signature))
					return c;

			return null;
		}

		public static List<Type> Repeat(this Type t, int times)
		{
			var types = new List<Type>();
			for (var i = 0; i < times; i++) types.Add(t);
			return types;
		}

		private static bool TypesMatch(ParameterInfo[] parameters, List<Type> signature)
		{
			if (parameters.Length != signature.Count) return false;

			for (var i = 0; i < parameters.Length; i++)
				if (parameters[i].ParameterType != signature[i])
					return false;

			return true;
		}

		public static MethodInfo FindMatchingMethod(this Type t, string methodName, List<Type> signature, Type returnType)
		{
			foreach (var m in t.GetMethods())
				if (m.Name == methodName)
					if (TypesMatch(m.GetParameters(), signature))
						if (m.ReturnType == returnType)
							return m;

			return null;
		}
	}
}