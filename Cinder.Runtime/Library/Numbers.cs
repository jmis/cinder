namespace Cinder.Runtime.Library
{
	public class Numbers
	{
		public static object Add(object x, object y)
		{
			return (int) x + (int) y;
		}

		public static object Divide(object x, object y)
		{
			return (int) x/(int) y;
		}

		public static object Mod(object x, object y)
		{
			return (int) x%(int) y;
		}

		public static object Subtract(object x, object y)
		{
			return (int) x - (int) y;
		}

		public static object LessThan(object x, object y)
		{
			return (int) x < (int) y;
		}

		public static object Even(object x)
		{
			return ((int) x)%2 == 0;
		}
	}
}