using System;
using Cinder.Runtime.Sequences;

namespace Cinder.Runtime.Library
{
	public class SequenceHelpers
	{
		public static object Count(object obj)
		{
			var seq = obj as Sequence;
			if (obj == null) throw new Exception("Cannot call Count on null object.");
			if (seq == null) throw new Exception("Count requires a sequence.");
			return seq.CountSequence();
		}

		public static object IsEmpty(object obj)
		{
			var seq = obj as Sequence;
			if (obj == null) throw new Exception("Cannot call IsEmpty on null object.");
			if (seq == null) throw new Exception("IsEmpty requires a sequence.");
			return seq.IsEmpty();
		}

		public static object First(object obj)
		{
			var seq = obj as Sequence;
			if (obj == null) return null;
			if (seq == null) throw new Exception("First requires a sequence.");
			return seq.First();
		}

		public static object Rest(object obj)
		{
			var seq = obj as Sequence;
			if (obj == null) return null;
			if (seq == null) throw new Exception("Rest requires a sequence.");
			return seq.Rest();
		}

		public static object Cons(object objFirst, object objSeq)
		{
			if (objSeq == null) throw new Exception("Cannot call cons on null sequence.");
			var seq = objSeq as Sequence;
			if (seq == null) throw new Exception("Cons requires a sequence.");
			return new ConsList(objFirst, seq);
		}
	}
}