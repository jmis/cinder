(deftest "+ adds numbers"
	(= 2 (+ 1 1)))

(deftest "empty? returns true when collection is empty"
	(= true (empty? (list))))

(deftest "empty? returns false when collection has elements"
	(= false (empty? (list 1))))

(deftest "eq? returns true when two numbers are equivalent."
	(= true (eq? 1 1)))
	
(deftest "eq? returns false when two numbers are different."
	(= false (eq? 1 2)))

(deftest "list? returns true when parameter is list"
	(= true (list? (list))))
	
(deftest "list? returns false when parameter is not list"
	(= false (list? [])))
	
(deftest "list? returns false when parameter is null"
	(= false (list? null)))
	
(deftest "count returns the number of elements in a list"
	(= 5 (count (list 1 2 3 4 5))))
	
(deftest "count returns 0 when the list is empty"
	(= 0 (count (list))))
	
(deftest "count returns the number of elements in a vector"
	(= 2 (count [1 2])))
	
(deftest "first returns the first element in a list"
	(= 1 (first (list 1 2 3))))
	
(deftest "first returns null if the list is empty"
	(= null (first (list))))
	
(deftest "first returns null if the vector is empty"
	(= null (first [])))
	
(deftest "first returns null when passed null"
	(= null (first null)))
	
(deftest "rest returns null when passed null"
	(= null (rest null)))
	
(deftest "rest returns empty list when passed empty sequence"
	(= (list) (rest (list))))
	
(deftest "rest returns the elements in a sequence except for the first"
	(= (list 2) (rest (list 1 2))))
	
(deftest "rest returns empty from sequence with one element"
	(= (list) (rest (list 1))))
	
(deftest "empty sequence equality"
	(= (list) (list)))
	
(deftest "sequence equality"
	(= (list 1 2) (list 1 2)))
	
(deftest "vector and list equality"
	(= (list 1 2) [1 2]))
	
(deftest "vector and list non-equality"
	(not= (list 1 2) [2 1]))
	
(deftest "filter returns empty list when no matching elements"
	(empty? (filter (fn [x] (= 10 x)) (list 1 2 3 4 5))))
	
(deftest "filter returns single element"
	(= (list 1) (filter (fn [x] (= 1 x)) (list 1 2 3 4 5))))
	
(deftest "filter returns empty list when given empty list"
	(empty? (filter (fn [x] (= 1 x)) (list))))
	
(deftest "filter returns all matching elements"
	(= (list 2 4) (filter even? (list 1 2 3 4 5))))
	
(deftest "< returns true when first number is less than second"
	(< 3 4))
	
(deftest "< returns false when first number is greater than second"
	(false? (< 4 3)))
	
(deftest "< returns false when both numbers are equal"
	(false? (< 4 4)))
	
(deftest "some returns null when no matching elements"
	(null? (some even? (list 1 3 5))))
	
(deftest "some returns first matching element"
	(= 2 (some even? (list 2 4 6))))