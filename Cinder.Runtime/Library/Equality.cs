namespace Cinder.Runtime.Library
{
	public class Equality
	{
		public static object AreEqual(object obj1, object obj2)
		{
			if (obj1 == obj2) return true;
			if (obj1 == null) return false;
			if (obj2 == null) return false;
			return obj1.Equals(obj2);
		}
	}
}