(def list (fn [& elements]
	(new Cinder.Runtime.Sequences.LispList elements)))

(def vec (fn [& elements]
	(new Cinder.Runtime.Sequences.LispVector elements)))

(def defmacro (macro [name args body]
	(list (quote def) name (list (quote macro) args body))))

(defmacro defn [name args body]
	(list (quote def) name (list (quote fn) args body)))

(defmacro lazy-list [body]
	(list
		(quote new)
		(quote Cinder.Runtime.Sequences.LazyList)
		(list (quote fn) (quote []) body)))

(defn eval [form] (. Cinder.Compiler.Compilation Eval form))

(defn eq? [x y] (. Cinder.Runtime.Library.Equality AreEqual x y))

(defn = [x y] (. Cinder.Runtime.Library.Equality AreEqual x y))

(defn null? [x] (= x null))

(defn empty? [coll] (. Cinder.Runtime.Library.SequenceHelpers IsEmpty coll))

(defn first [seq] (. Cinder.Runtime.Library.SequenceHelpers First seq))

(defn rest [seq] (. Cinder.Runtime.Library.SequenceHelpers Rest seq))
				
(defn identity [x] x)

(defn second [coll]
	(first (rest coll)))

(defn last [coll]
	(if (empty? (rest coll))
		(first coll)
		(last (rest coll))))

(defn count [coll] (. Cinder.Runtime.Library.SequenceHelpers Count coll))

(defn not [x] (if x false true))

(defn not= [x y] (not (= x y)))

(defn list? [x] (not= (instance-of? Cinder.Runtime.Sequences.LispList x) null))

(defn vector? [x] (not= (instance-of? Cinder.Runtime.Sequences.LispVector x) null))

(defn seq? [x] (not= (instance-of? Cinder.Runtime.Sequences.Sequence x) null))

(defn + [x y] (. Cinder.Runtime.Library.Numbers Add x y))

(defn - [x y] (. Cinder.Runtime.Library.Numbers Subtract x y))

(defn / [x y] (. Cinder.Runtime.Library.Numbers Divide x y))

(defn % [x y] (. Cinder.Runtime.Library.Numbers Mod x y))

(defn < [x y] (. Cinder.Runtime.Library.Numbers LessThan x y))

(defn false? [x] (not x))

(defn inc [x] (+ x 1))

(defn even? [x] (. Cinder.Runtime.Library.Numbers Even x))

(defn str [x] (. x ToString))

(defn str-concat [x y] (. System.String Concat x y))

(defn cons [element seq] (. Cinder.Runtime.Library.SequenceHelpers Cons element seq))

(defn map [f coll]
	(lazy-list
		(if (empty? coll)
				(list)
			(cons (f (first coll)) (map f (rest coll))))))

(defn append-file [filename text]
	(. System.IO.File AppendAllText filename text))

(defn write-file [filename text]
	(. System.IO.File WriteAllText filename text))

(defmacro let [bindings body]
	(if (not (even? (count bindings)))
		(throw (new System.Exception "Let must have an even number of bindings."))
		(list
			(quote def-local)
			(first bindings)
			(second bindings)
			(if (empty? (rest (rest bindings)))
				body
				(list (quote let) (rest (rest bindings)) body)))))
				
(defmacro cond [& clauses]
	(if (not (empty? clauses))
		(list
			(quote if)
			(first clauses)
			(if (not (empty? (rest clauses)))
				(second clauses)
				(throw (new System.Exception "Cond must have an even number of clauses.")))
			(cons (quote cond) (rest (rest clauses))))))

(defn range-infinite [start]
	(cons start (lazy-list (range-infinite (inc start)))))
			
(defn range [start stop]
	(lazy-list
		(if (eq? start stop)
			(list start)
			(cons start (range (+ start 1) stop)))))

(defn range_recur [start stop acc]
	(if (eq? start stop)
		(cons start acc)
		(range_recur (+ start 1) stop (cons start acc))))

(defn reduce [f coll]
	(if (eq? (count coll) 1)
		(first coll)
		(f (first coll) (reduce f (rest coll)))))

(defn filter [pred s]
	(lazy-list
		(if (empty? s) (list)
		(let [f (first s) r (rest s)]
			(if (pred f)
				(cons f (filter pred r))
				(filter pred r))))))

(defn some [pred s]
	(first (filter pred s)))

(defn do [& results]
	(last results))

(defn foreach [f coll]
	(do
		(f (first coll))
		(if (not (empty? (rest coll)))
			(foreach f (rest coll)))))

(defn print [x]
	(. System.Console Write (str x)))

(defn println [& more]
	(. System.Console WriteLine (reduce str-concat more)))

(defn time [f]
	(let [stop-watch (. System.Diagnostics.Stopwatch StartNew)]
		(let [function-result (f)
			  result-count (if (seq? function-result) (count function-result))]
				(vec function-result (. stop-watch get_ElapsedMilliseconds)))))
