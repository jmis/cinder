(def all-tests (list))

(defmacro deftest [name body]
	(list
		(quote def)
		(quote all-tests)
		(list
			(quote cons)
			(vec name (list (quote fn) [] body))
			(quote all-tests))))

(defn run-test [test]
	(vec
		(first test)
		((last test))))

(defn execute-tests []
	(filter (fn [result] (false? (second result))) (map run-test all-tests)))
	
(defn run-all-tests []
	(let [run-result (time execute-tests)
		  failed-tests (first run-result)
		  elapsed-time (last run-result)]
		(if (empty? failed-tests)
			(println (count all-tests) " tests passed")
			(foreach println (map first failed-tests)))))
			