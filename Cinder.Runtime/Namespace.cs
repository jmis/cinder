using System.Collections.Generic;
using System.Linq;

namespace Cinder.Runtime
{
	public class Namespace
	{
		private static readonly Dictionary<string, Var> Vars = new Dictionary<string, Var>();

		public static Var Create(string name)
		{
			if (!Vars.ContainsKey(name)) Vars.Add(name, new Var(name));
			return Vars[name];
		}

		public static Var Get(string name)
		{
			return Vars[name];
		}

		public static List<Var> GetAll()
		{
			return Vars.Values.ToList();
		}
	}
}