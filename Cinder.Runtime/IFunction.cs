using System;
using System.Collections.Generic;
using Cinder.Runtime.Sequences;
using System.Linq;

namespace Cinder.Runtime
{
	public interface IFunction
	{
		bool IsMacro();
		object Apply();
		object Apply(object arg1);
		object Apply(object arg1, object arg2);
		object Apply(object arg1, object arg2, object arg3);
		object Apply(object arg1, object arg2, object arg3, object arg4);
		object Apply(object arg1, object arg2, object arg3, object arg4, params object[] args);
	}

	public abstract class UserDefinedFunction : FunctionAdapter
	{
		public override bool IsMacro()
		{
			return false;
		}
	}

	public abstract class UserDefinedVarArgsFunction : VarArgsFunctionAdapter
	{
		public override bool IsMacro()
		{
			return false;
		}
	}

	public abstract class UserDefinedMacro : FunctionAdapter
	{
		public override bool IsMacro()
		{
			return true;
		}
	}

	public abstract class UserDefinedVarArgsMacro : VarArgsFunctionAdapter
	{
		public override bool IsMacro()
		{
			return true;
		}
	}

	public abstract class FunctionAdapter : IFunction
	{
		public abstract bool IsMacro();

		public virtual object Apply()
		{
			throw new ArgumentException("Not implemented.");
		}

		public virtual object Apply(object arg1)
		{
			throw new ArgumentException("Invalid number of arguments.");
		}

		public virtual object Apply(object arg1, object arg2)
		{
			throw new ArgumentException("Invalid number of arguments.");
		}

		public virtual object Apply(object arg1, object arg2, object arg3)
		{
			throw new ArgumentException("Invalid number of arguments.");
		}

		public virtual object Apply(object arg1, object arg2, object arg3, object arg4)
		{
			throw new ArgumentException("Invalid number of arguments.");
		}

		public virtual object Apply(object arg1, object arg2, object arg3, object arg4, params object[] args)
		{
			throw new ArgumentException("Invalid number of arguments.");
		}
	}

	public abstract class VarArgsFunctionAdapter : IFunction
	{
		public abstract bool IsMacro();
		public abstract int GetMandatoryArity();

		private void throwArityException(int givenArity)
		{
			throw new ArgumentException("Function does not support arity of: " + givenArity);
		}

		public virtual object Apply() 
		{
			switch (GetMandatoryArity())
			{
				case 0: return ApplyVarArgs(new LispList()); break;
				default: throwArityException(0); break;
			}

			return null;
		}

		public virtual object Apply(object arg1)
		{
			switch (GetMandatoryArity())
			{
				case 0: return ApplyVarArgs(new LispList(new List<object>() { arg1 })); break;
				case 1: return ApplyVarArgs(arg1, new LispList()); break;
				default: throwArityException(1); break;
			}

			return null;
		}

		public virtual object Apply(object arg1, object arg2)
		{
			switch (GetMandatoryArity())
			{
				case 0: return ApplyVarArgs(new LispList(new List<object>() { arg1, arg2 })); break;
				case 1: return ApplyVarArgs(arg1, new LispList(new List<object>() { arg2 })); break;
				case 2: return ApplyVarArgs(arg1, arg2, new LispList()); break;
				default: throwArityException(2); break;
			}

			return null;
		}

		public virtual object Apply(object arg1, object arg2, object arg3)
		{
			switch (GetMandatoryArity())
			{
				case 0: return ApplyVarArgs(new LispList(new List<object>() { arg1, arg2, arg3 })); break;
				case 1: return ApplyVarArgs(arg1, new LispList(new List<object>() { arg2, arg3 })); break;
				case 2: return ApplyVarArgs(arg1, arg2, new LispList(new List<object>() { arg3 })); break;
				case 3: return ApplyVarArgs(arg1, arg2, arg3, new LispList()); break;
				default: throwArityException(3); break;
			}

			return null;
		}

		public virtual object Apply(object arg1, object arg2, object arg3, object arg4)
		{
			switch (GetMandatoryArity())
			{
				case 0: return ApplyVarArgs(new LispList(new List<object>() { arg1, arg2, arg3, arg4 })); break;
				case 1: return ApplyVarArgs(arg1, new LispList(new List<object>() { arg2, arg3, arg4 })); break;
				case 2: return ApplyVarArgs(arg1, arg2, new LispList(new List<object>() { arg3, arg4 })); break;
				case 3: return ApplyVarArgs(arg1, arg2, arg3, new LispList(new List<object>() { arg4 })); break;
				case 4: return ApplyVarArgs(arg1, arg2, arg3, arg4, new LispList()); break;
				default: throwArityException(4); break;
			}

			return null;
		}

		public virtual object Apply(object arg1, object arg2, object arg3, object arg4, params object[] args)
		{
			var allArgs = new List<object>();
			allArgs.Add(arg1);
			allArgs.Add(arg2);
			allArgs.Add(arg3);
			allArgs.Add(arg4);
			allArgs.AddRange(args);

			var argSequence = new LispList(allArgs);
			
			switch (GetMandatoryArity())
			{
				case 0: return ApplyVarArgs(argSequence); break;
				case 1: return ApplyVarArgs(arg1, argSequence.Rest()); break;
				case 2: return ApplyVarArgs(arg1, arg2, argSequence.Rest().Rest()); break;
				case 3: return ApplyVarArgs(arg1, arg2, arg3, argSequence.Rest().Rest().Rest()); break;
				case 4: return ApplyVarArgs(arg1, arg2, arg3, arg4, argSequence.Rest().Rest().Rest().Rest()); break;
				default: throwArityException(4); break;
			}

			return null;
		}

		public virtual object ApplyVarArgs(Sequence rest) { throw new ArgumentException("Invalid number of arguments."); }
		public virtual object ApplyVarArgs(object arg1, Sequence rest) { throw new ArgumentException("Invalid number of arguments."); }
		public virtual object ApplyVarArgs(object arg1, object arg2, Sequence rest) { throw new ArgumentException("Invalid number of arguments."); }
		public virtual object ApplyVarArgs(object arg1, object arg2, object arg3, Sequence rest) { throw new ArgumentException("Invalid number of arguments."); }
		public virtual object ApplyVarArgs(object arg1, object arg2, object arg3, object arg4, Sequence rest) { throw new ArgumentException("Invalid number of arguments."); }
	}

	public class FunctionHelpers
	{
		public const int MaxArity = 4;

		public static object ApplyTo(IFunction function, List<object> args)
		{
			if (args.Count == 0) return function.Apply();
			if (args.Count == 1) return function.Apply(args[0]);
			if (args.Count == 2) return function.Apply(args[0], args[1]);
			if (args.Count == 3) return function.Apply(args[0], args[1], args[2]);
			if (args.Count == 4) return function.Apply(args[0], args[1], args[2], args[3]);
			return function.Apply(args[0], args[1], args[2], args[3], args.GetRange(4, args.Count - 4));
		}

		public static Type[] CreateSignature(int arity)
		{
			var signature = new[] {typeof (object), typeof (object), typeof (object), typeof (object), typeof (object[])};
			if (arity == 0) signature = new Type[0];
			if (arity == 1) signature = new[] {typeof (object)};
			if (arity == 2) signature = new[] {typeof (object), typeof (object)};
			if (arity == 3) signature = new[] {typeof (object), typeof (object), typeof (object)};
			if (arity == 4) signature = new[] {typeof (object), typeof (object), typeof (object), typeof (object)};
			return signature;
		}
	}
}