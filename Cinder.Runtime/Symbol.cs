namespace Cinder.Runtime
{
	public class Symbol
	{
		private readonly string _name;

		public Symbol(string name)
		{
			_name = name;
		}

		public string Name
		{
			get { return _name; }
		}

		public override string ToString()
		{
			return Name;
		}
	}
}