using System;
using Cinder.Runtime.Sequences;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cinder.Runtime
{
	public class Map : Sequence
	{
		private Dictionary<object, object> _internalMap;

		public Map(Dictionary<object, object> internalMap)
		{
			_internalMap = internalMap;
		}

		public static Map ParseMap(List<object> mapAsList)
		{
			return ParseMap(mapAsList.ToArray());
		}

		public static Map ParseMap(object[] content)
		{
			var dict = new Dictionary<object, object>();

			for (int i=0; i<content.Length; i = i + 2)
			{
				dict.Add(content[i], content[i+1]);
			}

			return new Map(dict);
		}

		public override object First()
		{
			return _internalMap.First();
		}

		public override Sequence Rest()
		{
			return new LispList(_internalMap.Skip(1).Select(kvp => new LispVector(new object[] { kvp.Key, kvp.Value })).ToArray());
		}

		public override bool IsEmpty()
		{
			return _internalMap.Count == 0;
		}

		public override string ToString()
		{
			var output = new StringBuilder();
			output.Append("{");
			_internalMap.ToList().ForEach(o => output.Append(o.Key).Append(" ").Append(o.Value).Append(" "));
			if (output[output.Length - 1] == ' ') output.Remove(output.Length - 1, 1);
			output.Append("}");
			return output.ToString();
		}
	}
}

