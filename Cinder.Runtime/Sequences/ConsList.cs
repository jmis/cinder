using System.Linq;
using System.Text;

namespace Cinder.Runtime.Sequences
{
	public class ConsList : Sequence
	{
		private readonly object _firstElement;
		private readonly Sequence _restOfList;

		public ConsList(object firstElement, Sequence restOfList)
		{
			_firstElement = firstElement;
			_restOfList = restOfList;
		}

		public override object First()
		{
			return _firstElement;
		}

		public override Sequence Rest()
		{
			return _restOfList;
		}

		public override bool IsEmpty()
		{
			return false;
		}

		public override string ToString()
		{
			var output = new StringBuilder();
			output.Append("(");
			output.Append(_firstElement.ToString());
			var remainingElements = _restOfList.ToList();
			if (remainingElements.Count > 0) output.Append(" ");
			remainingElements.ForEach(o => output.Append(o.ToString()).Append(" "));
			if (output[output.Length - 1] == ' ') output.Remove(output.Length - 1, 1);
			output.Append(")");
			return output.ToString();
		}
	}
}