using System.Collections.Generic;
using System.Text;

namespace Cinder.Runtime.Sequences
{
	public class OffsetList : Sequence
	{
		private readonly List<object> _list;
		private readonly int _firstIndex;

		public OffsetList(List<object> list, int firstIndex)
		{
			_list = list;
			_firstIndex = firstIndex;
		}

		public override string ToString()
		{
			var output = new StringBuilder();
			output.Append("(");
			foreach (var element in this) output.Append(element.ToString()).Append(" ");
			if (output[output.Length - 1] == ' ') output.Remove(output.Length - 1, 1);
			output.Append(")");
			return output.ToString();
		}

		public override object First()
		{
			if (_firstIndex >= _list.Count) return null;
			return _list[_firstIndex];
		}

		public override Sequence Rest()
		{
			return new OffsetList(_list, _firstIndex + 1);
		}

		public override bool IsEmpty()
		{
			return _firstIndex >= _list.Count;
		}

		public override int CountSequence()
		{
			if (_firstIndex >= _list.Count) return 0;
			return _list.Count - _firstIndex;
		}

		public override object this[int index]
		{
			get { return _list[_firstIndex + index]; }
		}

		public override List<object> ConvertToList()
		{
			return _list.GetRange(_firstIndex, (int) CountSequence());
		}
	}
}