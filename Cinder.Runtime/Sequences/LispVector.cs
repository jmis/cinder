using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cinder.Runtime.Sequences
{
	public class LispVector : Sequence
	{
		private readonly List<object> _list;

		public LispVector() : this(new List<object>())
		{
		}

		//public LispVector(object obj)
		//    : this(new List<object>() { obj })
		//{
		//}

		public LispVector(object[] elements)
			: this(elements.ToList())
		{
		}

		public LispVector(List<object> list)
		{
			_list = list;
		}

		public LispVector(Sequence sequence) : this(sequence.ConvertToList())
		{
		}

		public override string ToString()
		{
			var output = new StringBuilder();
			output.Append("[");

			_list.ToList().ForEach(
				o =>
				{
					if (o == null) output.Append("null ");
					else output.Append(o.ToString()).Append(" ");
				});

			if (output[output.Length - 1] == ' ') output.Remove(output.Length - 1, 1);
			output.Append("]");
			return output.ToString();
		}

		public override object First()
		{
			if (IsEmpty()) return null;
			return _list[0];
		}

		public override Sequence Rest()
		{
			if (IsEmpty()) return this;
			return new OffsetList(_list, 1);
		}

		public override bool IsEmpty()
		{
			return _list.Count == 0;
		}

		public override int CountSequence()
		{
			return _list.Count;
		}

		public override object this[int index]
		{
			get { return _list[index]; }
		}

		public override List<object> ConvertToList()
		{
			return _list;
		}

		public override IEnumerator<object> GetEnumerator()
		{
			return _list.GetEnumerator();
		}
	}
}