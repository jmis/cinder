using System.Linq;
using System.Text;

namespace Cinder.Runtime.Sequences
{
	public class LazyList : Sequence
	{
		private readonly IFunction _elementGenerationFunction;
		private Sequence _nextSegment;
		private bool _hasElementCached;
		private object _cachedValue;

		public LazyList(object function)
		{
			_elementGenerationFunction = function as IFunction;
		}

		private void CalculateNextValue()
		{
			if (_hasElementCached) return;
			_hasElementCached = true;
			var lazyListEvaluationResult = _elementGenerationFunction.Apply() as Sequence;

			_cachedValue = lazyListEvaluationResult.First();
			_nextSegment = lazyListEvaluationResult.Rest();
		}

		public override object First()
		{
			CalculateNextValue();
			return _cachedValue;
		}

		public override Sequence Rest()
		{
			CalculateNextValue();
			return _nextSegment;
		}

		public override bool IsEmpty()
		{
			CalculateNextValue();
			return !(_nextSegment is LazyList);
		}

		public override string ToString()
		{
			var output = new StringBuilder();
			output.Append("(");

			foreach (var element in this.ToList())
			{
				if (element == null) output.Append("null").Append(" ");
				else output.Append(element.ToString()).Append(" ");
			}

			if (output[output.Length - 1] == ' ') output.Remove(output.Length - 1, 1);
			output.Append(")");
			return output.ToString();
		}
	}
}