using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Cinder.Runtime.Sequences
{
	public abstract class Sequence : IEnumerable<object>
	{
		public abstract object First();
		public abstract Sequence Rest();
		public abstract bool IsEmpty();

		public virtual int CountSequence()
		{
			var counter = 0;
			foreach (var o in this) counter++;
			return counter;
		}

		public virtual List<object> ConvertToList()
		{
			var list = new List<object>();
			list.AddRange(this);
			return list;
		}

		public virtual IEnumerator<object> GetEnumerator()
		{
			var currentSequence = this;

			while (!currentSequence.IsEmpty())
			{
				yield return currentSequence.First();
				currentSequence = currentSequence.Rest();
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public virtual object this[int index]
		{
			get { return this.ElementAt(index); }
		}

		public override bool Equals(object obj)
		{
			return ValuesEqual(obj);
		}

		public override int GetHashCode()
		{
			return ConvertToList().GetHashCode();
		}

		public virtual bool ValuesEqual(object two)
		{
			if (ReferenceEquals(two, null)) return false;
			if (ReferenceEquals(this, two)) return true;
			var twoAsSequence = two as Sequence;
			if (twoAsSequence == null) return false;

			var enumOne = GetEnumerator();
			var enumTwo = twoAsSequence.GetEnumerator();

			while (enumOne.MoveNext() && enumTwo.MoveNext())
			{
				if (!enumOne.Current.Equals(enumTwo.Current))
				{
					return false;
				}
			}

			if (!enumOne.MoveNext() && !enumTwo.MoveNext()) return true;
			return false;
		}
	}
}