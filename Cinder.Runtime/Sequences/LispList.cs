using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cinder.Runtime.Sequences
{
	public class LispList : Sequence
	{
		private readonly List<object> _list;

		public LispList()
		{
			_list = new List<object>();
		}

		public LispList(List<object> sequence)
		{
			_list = sequence;
		}

		public LispList(object[] elements) : this(elements.ToList())
		{
		}

		public LispList(Sequence sequence) : this(sequence.ConvertToList())
		{
		}

		public override string ToString()
		{
			var output = new StringBuilder();
			output.Append("(");
			_list.ForEach(o => output.Append(o).Append(" "));
			if (output[output.Length - 1] == ' ') output.Remove(output.Length - 1, 1);
			output.Append(")");
			return output.ToString();
		}

		public override object First()
		{
			if (_list.Count == 0) return null;
			return _list[0];
		}

		public override Sequence Rest()
		{
			if (_list.Count == 0) return this;
			return new OffsetList(_list, 1);
		}

		public override bool IsEmpty()
		{
			return _list.Count == 0;
		}

		public override int CountSequence()
		{
			return _list.Count;
		}

		public override object this[int index]
		{
			get { return _list[index]; }
		}

		public override List<object> ConvertToList()
		{
			return _list;
		}

		public override IEnumerator<object> GetEnumerator()
		{
			return _list.GetEnumerator();
		}
	}
}